## Scenario

### Add contacts to scenario

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Scenario;

$ipresso = new \iPresso();

$key = 'key';
$contacts = [1,2];
$contactsData = ['1' => ['key'=>'value'],'2' => ['key2'=>'value2']];
$scenarioData = ['key' => 'value'];

$scenario = new Scenario($contacts, $contactsData, $scenarioData);

/** @var Response $response */
$response = $ipresso->scenario->addContacts($key, $scenario);

$success = $response->code === 200;
```

### Start scenario

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$key = 'key';

/** @var Response $response */
$response = $ipresso->scenario->startScenario($key);

$success = $response->code === 200;
```

### Hold scenario

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$key = 'key';

/** @var Response $response */
$response = $ipresso->scenario->holdScenario($key);

$success = $response->code === 200;
```
