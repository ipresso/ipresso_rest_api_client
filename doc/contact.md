
## Contact

### Adding new contact

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Contact;

$ipresso = new \iPresso();

$contacts = new Contact(email: 'my@contact.com');//or [new Contact()]
$contacts->setFirstName('firstName');

/** @var Response $response */
$response = $ipresso->contact->add($contacts);

$success = $response->code === 200;
$data = $response->getData()['contact'];
$first = array_shift($data);

$alreadyExists = $first['code'] === 303;
$created = $first['code'] === 201;
```

### Edit contact

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Contact;

$ipresso = new \iPresso();
$idContact = 1;
$contacts = new Contact(email: 'my@contact.com');//or [new Contact()]
$contacts->setFirstName('firstName');

/** @var Response $response */
$response = $ipresso->contact->edit($idContact, $contacts);

$success = $response->code === 201;
```

### Delete contact

```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();
$idContact = 1;

/** @var Response $response */
$response = $ipresso->contact->delete($idContact);

$success = $response->code === 200;
```


### Get contact details

```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();
$idContact = 1;

/** @var Response $response */
$response = $ipresso->contact->get($idContact);

$success = $response->code === 200;
$contact = $response->getData()['contact'];
```

### Adding tags to contact

```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();
$idContact = 1;
$tag = 'tag';

/** @var Response $response */
$response = $ipresso->contact->addTag($idContact, $tag);

$success = $response->code === 200;
```

### Get contact tags

```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();
$idContact = 1;

/** @var Response $response */
$response = $ipresso->contact->getTag($idContact);

$success = $response->code === 200;
$tags = $response->getData()['tag'];
```

### Remove tag assigment from contact

```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();
$idContact = 1;
$tagId = 1;

/** @var Response $response */
$response = $ipresso->contact->deleteTag($idContact, $tagId);

$success = $response->code === 200;
```

### Assign categories to contact

```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idContact = 1;
$categories = [1, 2, 3];

/** @var Response $response */
$response = $ipresso->contact->addCategory($idContact, $categories);
$success = $response->code === 200;
```

### Get contact categories

```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idContact = 1;

/** @var Response $response */
$response = $ipresso->contact->getCategory($idContact);

$success = $response->code === 200;
$categories = $response->getData()['category'];
```

### Remove category assigment from contact

```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idContact = 1;
$idCategory = 2;

/** @var Response $response */
$response = $ipresso->contact->deleteCategory($idContact, $idCategory);

$success = $response->code === 200;
```

### Get integration of the contact
#### deprecated
```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idContact = 1;

/** @var Response $response */
$response = $ipresso->contact->getIntegration($idContact);

$success = $response->code === 200;
$integrations = $response->getData()['integration'];
```

### Assign agreements to contact

```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idContact = 1;
// [id => status (1 - agreement, 2 - withdrawal)]
$agreements = [1 => 1, 2 => 1];

/** @var Response $response */
$response = $ipresso->contact->addAgreement($idContact, $agreements);
$success = $response->code === 200;
```

### Withdrawal of consent

```php
<?php

use \iPresso\Model\Contact;
use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idContact = 1;
$agreement = 1;

/** @var Response $response */
$response = $ipresso->contact->deleteAgreement($idContact, $agreements);
$success = $response->code === 200;
```

### Get contact consents

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idContact = 1;

/** @var Response $response */
$response = $ipresso->contact->getAgreement($idContact);
$success = $response->code === 200;
$agreements = $response->getData()['agreement'];
```

### Adding activity to contact

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$idContact = 1;
$contactActivity = new ContactActivity('key' );
$contactActivity->addParameter('parameterKey','parameterValue');

/** @var Response $response */
$response = $ipresso->contact->addActivity($idContact, $contactActivity);

$success = $response->code === 200;
```
### Adding activities to contact

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$idContact = 1;
$contactActivity1 = new ContactActivity('key' );
$contactActivity1->addParameter('parameterKey','parameterValue');
$contactActivity2 = new ContactActivity('key2' );
$contactActivity2->addParameter('parameterKey2','parameterValue2');
$activities = [$contactActivity1, $contactActivity2];

/** @var Response $response */
$response = $ipresso->contact->addActivities($idContact, $activities);

$success = $response->code === 200;
```
### Get contact activities

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idContact = 1;
// 50 rows per page
$page = 1;

/** @var Response $response */
$response = $ipresso->contact->getActivity($idContact, $page);

$success = $response->code === 200;
$activities = $response->getData()['activity'];
```

### Add action to contact

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactAction;

$ipresso = new \iPresso();

$idContact = 1;
$contactAction = new ContactAction('actionKey');
$contactAction->addParameter('paramKey', 'paramValue');

/** @var Response $response */
$response = $ipresso->contact->addAction($idContact, $contactAction);

$success = $response->code === 200;
```
### Get contact actions

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idContact = 1;
// 50 rows per page
$page = 1;

/** @var Response $response */
$response = $ipresso->contact->getAction($idContact, $page);

$success = $response->code === 200;
$actions = $response->getData()['action'];
```

### Get contact type

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$idContact = 1;

/** @var Response $response */
$response = $ipresso->contact->getType($idContact);

$contactType = $response->getData()['type'];
$success = $response->code === 200;
```

### Setting contact type

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$idContact = 1;
$key = 'typeKey';

/** @var Response $response */
$response = $ipresso->contact->setType($idContact, $key);

$success = $response->code === 200;
```

### Get connections between contacts

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Contact;

$ipresso = new \iPresso();

$idContact = 1;

/** @var Response $response */
$response = $ipresso->contact->getConnection($idContact);

$success = $response->code === 200;
$connections = $response->getData()['connection'];
```

### Connect contacts

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$idContact = 1;
$idContactToConnect = 2;

/** @var Response $response */
$response = $ipresso->contact->setConnection($idContact, $idContactToConnect);

$success = $response->code === 200;
```

### Delete connections

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$idContact = 1;
$idChild = 2;

/** @var Response $response */
$response = $ipresso->contact->deleteConnection($idContact, $idChild);

$success = $response->code === 200;
```

### Bulk addition of activities to contacts

```php
<?php

use \iPresso\Model\ContactActivity;
use \iPresso\Service\Response;
use \iPresso\Model\MassContactActivity;

$ipresso = new \iPresso();

$contactId = 1;
$massActivity = new MassContactActivity([]);
$massActivity->addContactActivity($contactId, new ContactActivity('activityKey'));

/** @var Response $response */
$response = $ipresso->contact->addMassActivity($massActivity);

$success = $response->code === 200;
```

### Bulk addition of actions to contacts

```php
<?php

use \iPresso\Model\ContactAction;
use \iPresso\Service\Response;
use \iPresso\Model\MassContactAction;

$ipresso = new \iPresso();

$contactId = 1;
$massAction = new MassContactAction([]); 
$massAction->addContactAction($contactId, new ContactAction('actionKey'));

/** @var Response $response */
$response = $ipresso->contact->addMassAction($massAction);

$success = $response->code === 200;
```

### Get profile pages

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$idContact = 1;

/** @var Response $response */
$response = $ipresso->contact->getProfilePage($idContact);

$success = $response->code === 200;
$profiles = $response->getData()['profilepages'];

```

### Get contact consents in group

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$idContact = 1;
$groupApiKey = 'key';

/** @var Response $response */
$response = $ipresso->contact->getAgreementsByGroup($idContact, $groupApiKey);

$success = $response->code === 200;
$consents = $response->getData()['agreement'];

```
