## Categories

### Add new category definition

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Category;

$ipresso = new \iPresso();

$category = new Category('name');
$category->setParentId(1);

/** @var Response $response */
$response = $ipresso->category->add($category);

$success = $response->code === 201;
```

### Get category definition
```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Category;

$ipresso = new \iPresso();

$categoryId = 1;

/** @var Response $response */
$response = $ipresso->category->get($categoryId);

$success = $response->code === 200;
$category = $response->getData()['category'];
```

### Edit category

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Category;

$ipresso = new \iPresso();

$category = new Category('name');
$categoryId = 2;
$category->setParentId(1);

/** @var Response $response */
$response = $ipresso->category->edit($categoryId, $category);

$success = $response->code === 200;
```

### Delete category

```php
<?php
use \iPresso\Service\Response;

$ipresso = new \iPresso();

$categoryId = `2`;

/** @var Response $response */
$response = $ipresso->category->delete($categoryId);

$success = $response->code === 200;
```

### Assign category to contacts

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$contacts = [1,2,3];
$categoryId = 2;

/** @var Response $response */
$response = $ipresso->category->addContact($categoryId, $contacts);

$success = $response->code === 200;
```

### Remove category assigment from contacts

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$contacts = [1,2,3];
$categoryId = 2;

/** @var Response $response */
$response = $ipresso->category->removeContacts($categoryId, $contacts);

$success = $response->code === 200;
```

### Remove category assigment from contact

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$contact = 1;
$categoryId = 2;

/** @var Response $response */
$response = $ipresso->category->deleteContact($categoryId, $contact);

$success = $response->code === 200;
```


### Get all contacts in category

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

// 50 rows per page
$page = 1;
$categoryId = 2;

/** @var Response $response */
$response = $ipresso->category->getContact($categoryId, $page);

$success = $response->code === 200;
$contacts = $response->getData()['id'];
```
