## Action definition

### Add new contact action definition

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Action;

$ipresso = new \iPresso();

$action = new Action('key', 'name');
$action->setParameter([
    [
        Action::VAR_NAME => 'paramName',
        Action::VAR_KEY => 'paramKey',
        Action::VAR_TYPE => Action::TYPE_DATETIME
    ],
    [
        Action::VAR_NAME => 'paramName2',
        Action::VAR_KEY => 'paramKey2',
        Action::VAR_TYPE => Action::TYPE_BOOL
    ]
]);

/** @var Response $response */
$response = $ipresso->action->add($action);

$success = $response->code === 201;
```

### Get available actions

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->action->get();

$success = $response->code === 200;
$actionsArray = $response->getData()['action'];
```

### Edit action definition

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Action;

$ipresso = new \iPresso();

$actionKey ='key-edit';
$action = new Action($actionKey, 'name-edit');
$action->setParameter([
    [
        Action::VAR_NAME => 'paramName-edit',
        Action::VAR_KEY => 'paramKey-edit',
        Action::VAR_TYPE => Action::TYPE_INTEGER
    ],
    [
        Action::VAR_NAME => 'paramName2-edit',
        Action::VAR_KEY => 'paramKey2-edit',
        Action::VAR_TYPE => Action::TYPE_STRING
    ]
]);

/** @var Response $response */
$response = $ipresso->action->edit($actionKey, $action);

$success = $response->code === 200;
```

### Delete action definition

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Action

$ipresso = new \iPresso();

$actionKey = 'key';

/** @var Response $response */
$response = $ipresso->action->delete($actionKey);

$success = $response->code === 200;
```
