## Contact activities

### Add new activity definition

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Activity;

$ipresso = new \iPresso();

$activity = new Activity('key', 'name');
$activity->addParameter(
    'paramName', 
    'paramKey', 
    Activity::TYPE_STRING
);

/** @var Response $response */
$response = $ipresso->activity->add($activity);

$success = $response->code === 201;
```

### Get available activities

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->activity->get();

$success = $response->code === 200;

$activities = $response->getData()['activity'];
```

### Edit activity definition

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$activityKey = 'key';
$activity = new Activity('key', 'name');
$activity->addParameter(
    'paramName', 
    'paramKey', 
    Activity::TYPE_STRING
);

/** @var Response $response */
$response = $ipresso->activity->edit($activityKey, $activity);

$success = $response->code === 200;
```

### Delete activity definition

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$activityKey = 'key';

/** @var Response $response */
$response = $ipresso->activity->delete($activityKey);

$success = $response->code === 200;
```

### Add activity to contacts

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$contactIds = [1, 2];
$parameters = [
    'paramKey' => 'value'
];

/** @var Response $response */
$response = $ipresso->activity->addContact(
    'activityKey', 
    $contactIds, 
    $parameters
);

$success = $response->code === 200;
```
