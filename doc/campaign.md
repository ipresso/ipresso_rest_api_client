## Campaigns

#### Depends on campaign configuration will be sent email, sms, voice or push campaign.

### Send campaign to contacts

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Campaign;

$ipresso = new \iPresso();

$campaignKey = 1 ?? 'api_key';
$campaign = new Campaign();
$campaign
    ->setIdContact([1, 2, 3])
    ->setContent([
        'bracket1' => 'Welcome', 
        'bracket2' => 'Content'
    ]);

/** @var Response $response */
$response = $ipresso->campaign->send($campaignKey, $campaign);

$success = $response->code === 200;
$campaignSendDetails = $response->getData();
```

### Send campaign using contact to provided email

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Campaign;

$ipresso = new \iPresso();

$campaignKey = 1 ?? 'api_key';

$email = new Campaign();
$email
    ->setIdContact([1 => 'email@email.com'])
    ->setContent([
        'bracket1' => 'Welcome', 
        'bracket2' => 'Content'
    ]);

/** @var Response $response */
$response = $ipresso->campaign->send($campaignKey, $email);

$success = $response->code === 200;
$campaignSendDetails = $response->getData();
```

### Send SMS/Voice using contact to provided mobile number

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Campaign;

$ipresso = new \iPresso();

$campaignKey = 1 ?? 'api_key';
$sms = new Campaign();
$sms
    ->setIdContact([1 => 123456789])
    ->setContent([
        'bracket1' => 'Welcome', 
        'bracket2' => 'Content'
    ]);

/** @var Response $response */
$response = $ipresso->campaign->send($campaignKey, $sms);

$success = $response->code === 200;
$campaignSendDetails = $response->getData();
```

### Send SMS / Voice to provided numbers

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Campaign;

$ipresso = new \iPresso();

$campaignKey = 1 ?? 'api_key';
$sms = new Campaign();
$sms
    ->setMobile([123456789, 987654321])
    ->setContent([
        'bracket1' => 'Welcome', 
        'bracket2' => 'Content'
    ]);

/** @var Response $response */
$response = $ipresso->campaign->send($campaignKey, $sms);

$success = $response->code === 200;
$campaignSendDetails = $response->getData();
```

### Send webpush / mobilepush / instantpush

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Campaign;

$ipresso = new \iPresso();

$campaignKey = 1 ?? 'api_key';
$push = new Campaign();
$push
    ->setToken(['qoeui23hkwuegrfu3t'])
    ->setHash(['Hash'])
    ->setContent([
        'bracket1' => 'Welcome', 
        'bracket2' => 'Content'
    ]);

/** @var Response $response */
$response = $ipresso->campaign->send($campaignKey, $push);

$success = $response->code === 200;
$campaignSendDetails = $response->getData();
```

### Send WhatsApp

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Campaign;

$ipresso = new \iPresso();

$campaignKey = 'api_key';
$campaign = new Campaign();
$campaign
    ->addIdContact($idContact)
    ->setContent([
        'bracket1' => 'Welcome', 
        'bracket2' => 'Content'
    ]);

/** @var Response $response */
$response = $ipresso->campaign->sendWhatsApp($campaignKey, $campaign);

$success = $response->code === 200;
$campaignSendDetails = $response->getData();
```
