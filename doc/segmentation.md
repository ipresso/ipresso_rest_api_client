## Segmentation

### Add new segment

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Segmentation;

$ipresso = new \iPresso();

$segmentation = new Segmentation(12, 'name');

/** @var Response $response */
$response = $ipresso->segmentation->add($segmentation);
$success = $response->code === 201;

```

### Get active segments

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->segmentation->get();

$success = $response->code === 200;
$segmentations = $response->getData()['segmentations'];
```

### Remove semgent

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idSegmentation = 1;

/** @var Response $response */
$response = $ipresso->segmentation->delete($idSegmentation);
$success = $response->code === 200;
```

### Add contact to segment

```php
<?php

use \iPresso\Service\Response;
use iPresso\Model\Segmentation;

$ipresso = new \iPresso();

$idSegmentation = 1;
$segment = new Segmentation(0,'name');
$segment->addContact(1);

/** @var Response $response */
$response = $ipresso->segmentation->addContact(
    $idSegmentation, 
    $segment
);
$success = $response->code === 201;
```
### Remove contact from segment

```php
<?php

use \iPresso\Service\Response;
use iPresso\Model\Segmentation;

$ipresso = new \iPresso();

$idSegmentation = 1;
$segment = new Segmentation(0,'name');
$segment->addContact(1);

/** @var Response $response */
$response = $ipresso->segmentation->removeContact(
    $idSegmentation, 
    $segment
);
$success = $response->code === 201;
```

```php
<?php

use \iPresso\Service\Response;
use iPresso\Model\Segmentation;

$ipresso = new \iPresso();

$idSegmentation = 1;

/** @var Response $response */
$response = $ipresso->segmentation->getContact($idSegmentation);
$success = $response->code === 200;
$contacts = $response->getData()['contact']
```
