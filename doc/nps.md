## NPS

### Get NPS data

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$npsApiKey = 'key';

/** @var Response $response */
$response = $ipresso->nps->get($npsApiKey);

$success = $response->code === 200;
$nps = $response->getData();
```

### Save NPS Answer

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\NpsAnswer;

$ipresso = new \iPresso();

$npsApiKey = 'key';
$score = 5;
$contactId = 1;
$npsAnswer = new NpsAnswer(
    $score,
    'answer',
    $contactId
);

/** @var Response $response */
$response = $ipresso->nps->saveAnswer($npsApiKey, $npsAnswer);
$success = $response->code === 200;
```
