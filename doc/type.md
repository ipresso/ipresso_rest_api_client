## Type

### Create new contact type

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Type;

$ipresso = new \iPresso();

$type = new Type('key', 'name');
$type->addAttribute('attribute_key');

/** @var Response $response */
$response = $ipresso->type->add($type);

$success = $response->code === 200;
```

### Get all contact types

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->type->get();
$typesWithAttributes = $response->getData()['type'];
```

### Get contacts with type

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idType = 1;

/** @var Response $response */
$response = $ipresso->type->getContact($idType);

$success = $response->code === 200;
$contacts = $response->getData()['id'];
```

### Assign contacts to type

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$typeKey = 'abc';
$contacts = [1, 2, 3];

/** @var Response $response */
$response = $ipresso->type->addContact($typeKey, $contacts);

$success = $response->code === 200;
```
