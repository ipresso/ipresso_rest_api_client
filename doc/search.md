## Search

### Search contacts

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Search;

$ipresso = new \iPresso();

$search = new Search();
$search->setFirstName('first_name');
$search->setLastName('last_name');
$search->setEmail('email_address');

/** @var Response $response */
$response = $ipresso->search->search($search)

$success = $response->code === 200;

$contacts = $response->getData()['contact'];
```

### Extended search contacts

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Search;

$search = new Search();
$search->setFirstName('fname');
$search->setLastName('lname');
$search->setEmail('email');

/** @var Response $response */
$response = $ipresso->search->search($search, true)

$success = $response->code === 200;
$contacts = $response->getData()['contact'];
```
