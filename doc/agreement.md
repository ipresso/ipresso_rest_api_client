## Agreements

### Add new agreement

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Agreement;

$ipresso = new \iPresso();

$name = 'newsletter consent';
$agreement = new Agreement($name);
$agreement
    ->setDescription('description')
    ->setDmStatus(Agreement::DIRECT_MARKETING_NON_VISIBLE);

/** @var Response $response */
$response = $ipresso->agreement->add($agreement);

$success = $response->code === 201;
```

### Get available agreements

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->agreement->get();

$success = $response->code === 200;

$consents = $response->getData()['agreement'];
```

### Edit agreement

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Agreement;

$ipresso = new \iPresso();

$idAgreement = 1;
$agreement = new Agreement('name-edit');
$agreement
    ->setDescription('description-edit')
    ->setDmStatus(Agreement::DIRECT_MARKETING_VISIBLE);

/** @var Response $response */
$response = $ipresso->agreement->edit($idAgreement, $agreement);

$success = $response->code === 200;
```

### Delete agreement

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idAgreement = 1;

/** @var Response $response */
$response = $ipresso->agreement->delete($idAgreement);

$success = $response->code === 200;
```

### Assign consent to contacts

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$idAgreement = 1;
$contacts = [1,2,3];

$response = $ipresso->agreement->addContact($idAgreement, $contacts);

$success = $response->code === 200;
```
