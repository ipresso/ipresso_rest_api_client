## Origin

### Get all origins

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->origin->get();

$success = $response->code === 200;
$origins = $response->getData()['origin'];
```

### Get contacts from origin

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idOrigin = 1;
// 50 rows per page
$page = 1;

/** @var Response $response */
$response = $ipresso->origin->getContact($idOrigin, $page);

$success = $response->code === 200;
$contacts = $response->getData()['id']
```
