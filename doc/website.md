## Monitoring websites

### Add monitoring website

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$url = 'www.ipresso.com';
$apiMethods = ['method1' => 1, 'method2'=> 0];

/** @var Response $response */
$response = $ipresso->www->add($url, $apiMethods);
$success = $response->code === 201;
```

### Get all monitored sites

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->www->get();

$success = $response->code === 200;
$sites = $response->getData()['www'];
```

### Get monitored site with details

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idWww = 1;

/** @var Response $response */
$response = $ipresso->www->get($idWww);

$success = $response->code === 200;
$sites = $response->getData()['www'];
```

### Delete monitored site

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idWww = 1;

/** @var Response $response */
$response = $ipresso->www->delete($idWww);
$success = $response->code === 200;
```
