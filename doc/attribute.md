## Contact and customer attributes

### Add new contact attribute definition

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Attribute;

$ipresso = new \iPresso();

$attribute = new Attribute('name', 'key', Attribute::TYPE_SELECT);
$attribute->setOption([
    'opt1-key' => 'opt1-name',
    'opt2-key' => 'opt2-name'
]);

/** @var Response $response */
$response = $ipresso->attribute->add($attribute);

$success = $response->code === 201;

```

### Get available attributes

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->attribute->get();

$success = $response->code === 200;
$attributes = $response->getData()['attribute'];
```

### Add new options to attribute

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\AttributeOption;

$ipresso = new \iPresso();


$attributeKey = 'key';
$attributeOption = new AttributeOption('key', 'name');

/** @var Response $response */
$response = $ipresso->attribute->addOption(
    $attributeKey, 
    $attributeOption
);
$success = $response->code === 201;
```

### Edit attribute option

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\AttributeOption;

$ipresso = new \iPresso();

$attributeKey = 'attr-key';
$attributeOption = new AttributeOption('opt-key', 'name-edit');

/** @var Response $response */
$response = $ipresso->attribute->editOption(
    $attributeKey, 
    $attributeOption
);

$success = $response->code === 200;
```

### Delete attribute option

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\AttributeOption;

$ipresso = new \iPresso();

$attributeKey = 'key';
$attributeOption = new AttributeOption('opt-key', 'name');

/** @var Response $response */
$response = $ipresso->attribute->deleteOption(
    $attributeKey, 
    $attributeOption
);

$success = $response->code === 200;
```
### Add new customer attribute definition

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\CustomerAttribute;

$ipresso = new \iPresso();

$attribute = new CustomerAttribute(
    'name', 
    'key', 
    1,
    CustomerAttribute::TYPE_INTEGER
);

/** @var Response $response */
$response = $ipresso->attribute->addCustomerAttribute($attribute);

$success = $response->code === 201;

```

### Get available customer attributes

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->attribute->getCustomerAttribute();

$success = $response->code === 200;
$attributes = $response->getData()['attribute'];
```

### Edit customer attribute

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\CustomerAttribute;

$ipresso = new \iPresso();

$attributeKey = 'attr-key';
$attribute = new CustomerAttribute(
    'name', 
    $attributeKey, 
    1,
    CustomerAttribute::TYPE_INTEGER
);

/** @var Response $response */
$response = $ipresso->attribute->editCustomerAttribute(
    $attributeKey, 
    $attribute
);

$success = $response->code === 200;
```

### Delete customer attribute

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\CustomerAttribute;

$ipresso = new \iPresso();

$attributeKey = 'attr-key';

/** @var Response $response */
$response = $ipresso->attribute->deleteCustomerAttribute($attributeKey);

$success = $response->code === 200;
```
