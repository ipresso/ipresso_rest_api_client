## Tag

### Add new tag

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Tag;

$ipresso = new \iPresso();

$tag = new Tag('name');

/** @var Response $response */
$response = $ipresso->tag->add($tag);

$success = $response->code === 200;
```

### Get all tags

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->tag->get();

$success = $response->code === 200;
$tags = $response->getData()['tag'];
```
### Get tag details

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

/** @var Response $response */
$response = $ipresso->tag->get(1);

$success = $response->code === 200;
$tags = $response->getData()['tag'];
```

### Delete tag

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idTag = 1;

/** @var Response $response */
$response = $ipresso->tag->delete($idTag);

$success = $response->code === 200;
```

### Edit tag

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Tag;

$ipresso = new \iPresso();

$idTag = 1;
$tag = new Tag('name');

/** @var Response $response */
$response = $ipresso->tag->edit($idTag, $tag);
$success = $response->code === 200;
```

### Assign tag to contacts

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\Tag;

$ipresso = new \iPresso();

$idTag = 1;
$contacts = [1, 2, 3];

/** @var Response $response */
$response = $ipresso->tag->addContact($idTag, $contacts);
$success = $response->code === 200;
```

### Get contacts

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idTag = 1;

/** @var Response $response */
$response = $ipresso->tag->getContact($idTag);

$success = $response->code === 200;
$tags = $response->getData()['id'];
```

### Remove tag assigment from contact

```php
<?php

use \iPresso\Service\Response;

$ipresso = new \iPresso();

$idTag = 1;
$idContact = 1;

/** @var Response $response */
$response = $ipresso->tag->deleteContact($idTag, $idContact);
$success = $response->code === 200;
```
