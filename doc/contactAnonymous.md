## Contact anonymous

### Adding activity to an anonymous contact

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;

$ipresso = new \iPresso();

$idContactAnon = 'hash';
$contactActivity = new ContactActivity('key');

/** @var Response $response */
$response = $ipresso->contactAnonymous->addActivity($idContactAnon, $contactActivity);

$success = $response->code === 200;
```

### Adding activities to an anonymous contact

```php
<?php

use \iPresso\Service\Response;
use \iPresso\Model\ContactActivity;
$ipresso = new \iPresso();

$idContactAnon = 'hash';
$contactActivity1 = new ContactActivity('key');
$contactActivity2 = new ContactActivity('key');

$activities = [$contactActivity1, $contactActivity2];

/** @var Response $response */
$response = $ipresso->contactAnonymous->addActivities(
    $idContactAnon, 
    $activities
);

$success = $response->code === 200;
```
