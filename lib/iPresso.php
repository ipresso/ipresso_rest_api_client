<?php

declare(strict_types=1);

use iPresso\Exception\ApiException;
use iPresso\Service\ActionService;
use iPresso\Service\ActivityService;
use iPresso\Service\AgreementService;
use iPresso\Service\AttributeService;
use iPresso\Service\CategoryService;
use iPresso\Service\CampaignService;
use iPresso\Service\ContactAnonymousService;
use iPresso\Service\ContactService;
use iPresso\Service\FormService;
use iPresso\Service\NpsService;
use iPresso\Service\Response;
use iPresso\Service\ScenarioService;
use iPresso\Service\SearchService;
use iPresso\Service\SegmentationService;
use iPresso\Service\OriginService;
use iPresso\Service\TagService;
use iPresso\Service\TypeService;
use iPresso\Service\WebsiteService;
use iPresso\Service\Service;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class iPresso
 */
class iPresso
{
    public ActionService $action;

    public ActivityService $activity;

    public AgreementService $agreement;

    public AttributeService $attribute;
    public CampaignService $campaign;

    public CategoryService $category;

    public ContactService $contact;

    public ContactAnonymousService $contactAnonymous;

    public ScenarioService $scenario;

    public SearchService $search;

    public SegmentationService $segmentation;

    public OriginService $origin;

    public TagService $tag;

    public TypeService $type;

    public WebsiteService $www;

    public NpsService $nps;
    public FormService $form;

    private Service $service;

    private Serializer $serializer;

    /**
     * iPresso constructor.
     */
    public function __construct()
    {
        $this->service = new Service();

        $this->serializer = self::buildSerializer();
        $this->action = new ActionService($this->service, $this->serializer);
        $this->activity = new ActivityService($this->service, $this->serializer);
        $this->agreement = new AgreementService($this->service);
        $this->attribute = new AttributeService($this->service, $this->serializer);
        $this->category = new CategoryService($this->service);
        $this->campaign = new CampaignService($this->service);
        $this->contact = new ContactService($this->service, $this->serializer);
        $this->contactAnonymous = new ContactAnonymousService($this->service);
        $this->scenario = new ScenarioService($this->service, $this->serializer);
        $this->search = new SearchService($this->service);
        $this->segmentation = new SegmentationService($this->service);
        $this->origin = new OriginService($this->service);
        $this->tag = new TagService($this->service);
        $this->type = new TypeService($this->service);
        $this->www = new WebsiteService($this->service);
        $this->nps = new NpsService($this->service, $this->serializer);
        $this->form = new FormService($this->service, $this->serializer);
    }

    public function setCustomerKey(string $customerKey): iPresso
    {
        $this->service->setCustomerKey($customerKey);
        return $this;
    }

    public function setLogin(string $login): iPresso
    {
        $this->service->setLogin($login);
        return $this;
    }

    public function setPassword(string $password): iPresso
    {
        $this->service->setPassword($password);
        return $this;
    }

    public function setToken(string $token): iPresso
    {
        $this->service->setToken($token);
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function setUrl(string $url): iPresso
    {
        $address = parse_url($url);
        if (!isset($address['scheme']) || $address['scheme'] != 'https') {
            throw new ApiException('Set URL with https://');
        }

        $this->service->setUrl($url);
        return $this;
    }

    public function setTokenCallBack(Closure $callBack): iPresso
    {
        $this->service->setTokenCallBack($callBack);
        return $this;
    }

    public function getToken(): Response|bool
    {
        return $this->service->getToken(true);
    }

    public function addHeader(string $header): iPresso
    {
        $this->service->addCustomHeader($header);
        return $this;
    }

    public function setExternalKey(string $key): iPresso
    {
        $this->service->addCustomHeader(Service::HEADER_EXTERNAL_KEY . $key);
        return $this;
    }

    public function debug(): iPresso
    {
        $this->service->debug();
        return $this;
    }

    public static function dump($die, $variable, bool $desc = false, bool $noHtml = false): void
    {
        if (is_string($variable)) {
            $variable = str_replace("<_new_line_>", "<BR>", $variable);
        }

        if ($noHtml) {
            echo "\n";
        } else {
            echo "<pre>";
        }

        if ($desc) {
            echo $desc . ": ";
        }

        print_r($variable);

        if ($noHtml) {
            echo "";
        } else {
            echo "</pre>";
        }

        if ($die) {
            die();
        }
    }

    /**
     * @return Serializer
     */
    public static function buildSerializer(): Serializer
    {
        $extractor = new PropertyInfoExtractor([], [new PhpDocExtractor(), new ReflectionExtractor()]);
        $normalizers = [
            new ArrayDenormalizer(),
            new DateTimeNormalizer([
                DateTimeNormalizer::FORMAT_KEY => 'Y-m-d H:i:s',
                DateTimeNormalizer::TIMEZONE_KEY => null,
            ]),
            new ObjectNormalizer(propertyAccessor: new PropertyAccessor(), propertyTypeExtractor:  $extractor),
        ];
        return new Serializer($normalizers, [new JsonEncoder()]);
    }
}
