<?php
declare(strict_types=1);

namespace iPresso\Model;

/**
 * Class CustomerAttribute
 * @package iPresso\Model
 */
class CustomerAttribute
{
    /**
     * CUSTOMER ATTRIBUTE TYPES
     */
    const TYPE_DECIMAL = 'decimal';
    const TYPE_INTEGER = 'integer';
    const TYPE_STRING = 'string';
    const TYPE_TEXT = 'text';

    private string $name;

    private string $key;

    private string $value;

    private string $type;

    public function __construct(string $name, string $key, string $value, string $type)
    {
        $this->name = $name;
        $this->key = $key;
        $this->value = $value;
        $this->type = $type;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CustomerAttribute
    {
        $this->name = $name;
        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): CustomerAttribute
    {
        $this->key = $key;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): CustomerAttribute
    {
        $this->value = $value;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): CustomerAttribute
    {
        $this->type = $type;
        return $this;
    }
}
