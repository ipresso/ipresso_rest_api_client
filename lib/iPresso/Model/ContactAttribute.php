<?php
declare(strict_types=1);

namespace iPresso\Model;

/**
 * Class ContactAttribute
 * @package iPresso\Model
 */
class ContactAttribute
{

    private string $key;

    private ?int $id;

    private string $type;

    private string $name;

    private array $options = [];
    private array $optionsByKey = [];

    public function __construct(
        string $key,
        string $type,
        string $name,
        int $id = null
    )
    {
        $this->key = $key;
        $this->type = $type;
        $this->name = $name;
        $this->id = $id;
    }


    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): static
    {
        $this->key = $key;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): static
    {
        $this->options = $options;
        return $this;
    }

    public function getOptionsByKey(): array
    {
        return $this->optionsByKey;
    }

    public function setOptionsByKey(array $optionsByKey): static
    {
        $this->optionsByKey = $optionsByKey;
        return $this;
    }
}
