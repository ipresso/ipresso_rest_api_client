<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

class Campaign
{
    const VAR_EMAIL = 'email';
    const VAR_ID_CONTACT = 'contactId';
    const VAR_MOBILE = 'phone';
    const VAR_CONTENT = 'content';
    const VAR_HASH = 'hash';
    const VAR_TOKEN = 'token';
    const VAR_FILES = 'files';

    public array $campaign = [];

    private array $content = [];

    private array $email = [];

    private array $idContact = [];

    private array $mobile = [];

    private array $hash = [];

    private array $token = [];

    private array $files = [];

    private array $validate = [
        self::VAR_CONTENT => self::VAR_CONTENT,
        self::VAR_EMAIL => self::VAR_EMAIL,
        self::VAR_ID_CONTACT => 'idContact',
        self::VAR_MOBILE => 'mobile',
        self::VAR_HASH => self::VAR_HASH,
        self::VAR_TOKEN => self::VAR_TOKEN,
        self::VAR_FILES => self::VAR_FILES
    ];

    public function getEmail(): array
    {
        return $this->email;
    }

    public function setEmail(array $email): Campaign
    {
        $this->email = $email;
        return $this;
    }

    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(array $content): Campaign
    {
        $this->content = $content;
        return $this;
    }

    public function getIdContact(): array
    {
        return $this->idContact;
    }

    public function setIdContact(array $idContact): Campaign
    {
        $this->idContact = $idContact;
        return $this;
    }

    public function getMobile(): array
    {
        return $this->mobile;
    }

    public function setMobile(array $mobile): Campaign
    {
        $this->mobile = $mobile;
        return $this;
    }

    public function addContent(string $bracket, string|array $value): Campaign
    {
        $this->content[$bracket] = $value;
        return $this;
    }

    public function addEmail(string $email): Campaign
    {
        $this->email[] = $email;
        return $this;
    }

    public function addMobile(int $mobile): Campaign
    {
        $this->mobile[] = $mobile;
        return $this;
    }

    public function addIdContact(int $idContact): Campaign
    {
        $this->idContact[] = $idContact;
        return $this;
    }

    public function getHash(): array
    {
        return $this->hash;
    }

    public function setHash(array $hash): Campaign
    {
        $this->hash = $hash;
        return $this;
    }

    public function addHash(string $hash): Campaign
    {
        $this->hash[] = $hash;
        return $this;
    }

    public function getToken(): array
    {
        return $this->token;
    }

    public function setToken(array $token): Campaign
    {
        $this->token = $token;
        return $this;
    }

    public function addToken(string $token): Campaign
    {
        $this->token[] = $token;
        return $this;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    public function setFiles(array $files): Campaign
    {
        $this->files = $files;
        return $this;
    }

    public function addFile(array $file): Campaign
    {
        $this->files[] = $file;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getCampaign(): array
    {
        foreach ($this->validate as $key => $value) {
            if (!empty($this->$value)) {
                $this->campaign[$key] = $this->$value;
            }
        }

        if (empty($this->campaign)) {
            throw new ApiException('No recipients in campaign');
        }

        return $this->campaign;
    }
}

