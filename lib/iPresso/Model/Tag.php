<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

/**
 * Class Tag
 * @package iPresso\Model
 */
class Tag
{
    const VAR_NAME = 'name';
    const VAR_PARENT_ID = 'parentId';

    public array $tag = [];

    /**
     * Parameter `TAG_NAME` should be replaced with the name of the tag being added.
     */
    private string $name;

    /**
     * Parameter `PARENT_TAG_ID` should be replaced with ID of parent tag.
     * Not required
     */
    private int $parentId = 0;

    /**
     * @param string $name
     */
    public function __construct(string $name = '')
    {
        $this->name = $name;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getParentId(): int
    {
        return $this->parentId;
    }

    public function setParentId(int $parentId): static
    {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getTag(): array
    {
        if (empty($this->name)) {
            throw new ApiException('Set tag name first.');
        }

        $this->tag[self::VAR_NAME] = $this->name;

        if (!empty($this->parentId)) {
            $this->tag[self::VAR_PARENT_ID] = $this->parentId;
        }

        return $this->tag;
    }
}
