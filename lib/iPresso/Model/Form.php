<?php

declare(strict_types=1);


namespace iPresso\Model;

use iPresso\Exception\ApiException;

class Form
{
    /**
     * @param array<string,int|float|bool|string|string[]|int[]|float[]|array> $fields field_key => value
     */
    public function __construct(
        public array $fields,
        public ?string $recaptchaToken = null,
    )
    {
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    public function setFields(array $fields): void
    {
        $this->fields = $fields;
    }

    public function getReCaptchaToken(): ?string
    {
        return $this->recaptchaToken;
    }

    public function setReCaptchaToken(?string $reCaptchaToken): void
    {
        $this->recaptchaToken = $reCaptchaToken;
    }

    /**
     * @throws ApiException
     */
    public function validate(): void
    {
        $prefixes = ['a_', 'c_', 'x_', 'f_', 'v_'];
        foreach ($this->fields as $field => $value) {
            if (!in_array(substr((string)$field, 0, 2), $prefixes)) {
                throw new ApiException(
                    'All field names must start with a prefix. Prefixes: ' . implode(', ', $prefixes)
                );
            }
        }
    }
}
