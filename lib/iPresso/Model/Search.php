<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

class Search
{
    const VAR_EMAIL = 'email';
    const VAR_FIRST_NAME = 'fname';
    const VAR_LAST_NAME = 'lname';
    const VAR_NAME = 'name';
    const VAR_PHONE = 'phone';
    const VAR_ID_CONTACT_FROM = 'idContactFrom';
    const VAR_ID_CONTACT_TO = 'idContactTo';
    const VAR_CREATE_DATE_FROM = 'createDateFrom';
    const VAR_CREATE_DATE_TO = 'createDateTo';
    const VAR_ID_CATEGORY = 'id_category';
    const VAR_TYPE = 'type';
    const VAR_TAG = 'tag';

    public array $search = [];

    /**
     * Original date of contact addition, in relation to which the search is to be conducted. Date format: YYYY-MM-YY; e.g. 2014-03-01
     */
    private string $createDateFrom = '';

    /**
     * Final date of contact addition, in relation to which the search is to be conducted. Date format: YYYY-MM-YY; e.g. 2014-03-01
     */
    private string $createDateTo = '';

    /**
     * Email address
     */
    private string $email = '';

    /**
     * First name
     */
    private string $firstName = '';

    /**
     * ID of category
     */
    private ?int $idCategory = null;

    /**
     *    Original ID of a contact, in relation to which the search is to be conducted.
     */
    private ?int $idContactFrom = null;

    /**
     * Final ID of a contact, in relation to which the search is to be conducted.
     */
    private ?int $idContactTo = null;

    /**
     * Last name
     */
    private string $lastName = '';

    /**
     * Name
     */
    private string $name = '';

    /**
     * Phone number
     */
    private string $phone = '';

    /**
     * Type of contact (enter type key)
     */
    private string $type = '';

    /**
     * Tag of contact (enter name of tag)
     */
    private string $tag = '';

    private array $validate = [
        self::VAR_FIRST_NAME => self::VAR_FIRST_NAME,
        self::VAR_LAST_NAME => self::VAR_LAST_NAME,
        self::VAR_NAME => self::VAR_NAME,
        self::VAR_EMAIL => self::VAR_EMAIL,
        self::VAR_PHONE => self::VAR_PHONE,
        self::VAR_ID_CONTACT_FROM => self::VAR_ID_CONTACT_FROM,
        self::VAR_ID_CONTACT_TO => self::VAR_ID_CONTACT_TO,
        self::VAR_CREATE_DATE_FROM => self::VAR_CREATE_DATE_FROM,
        self::VAR_CREATE_DATE_TO => self::VAR_CREATE_DATE_TO,
        self::VAR_ID_CATEGORY => self::VAR_ID_CATEGORY,
        self::VAR_TYPE => self::VAR_TYPE,
        self::VAR_TAG => self::VAR_TAG
    ];

    public function setTag(string $tag): static
    {
        $this->tag = $tag;
        return $this;
    }

    public function setCreateDateFrom(string $createDateFrom): static
    {
        $this->createDateFrom = $createDateFrom;
        return $this;
    }

    public function setCreateDateTo(string $createDateTo): static
    {
        $this->createDateTo = $createDateTo;
        return $this;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;
        return $this;
    }

    public function setFirstName(string $firstName): static
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function setIdCategory(int $idCategory): static
    {
        $this->idCategory = $idCategory;
        return $this;
    }

    public function setIdContactFrom(int $idContactFrom): static
    {
        $this->idContactFrom = $idContactFrom;
        return $this;
    }

    public function setIdContactTo(int $idContactTo): static
    {
        $this->idContactTo = $idContactTo;
        return $this;
    }

    public function setLastName(string $lastName): static
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function setPhone(string $phone): static
    {
        $this->phone = $phone;
        return $this;
    }

    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getCriteria(): array
    {

        foreach ($this->validate as $key => $value) {
            if (!empty($this->$value)) {
                $this->search[$key] = $this->$value;
            }
        }

        if (empty($this->search)) {
            throw new ApiException('Empty criteria.');
        }

        return $this->search;
    }
}
