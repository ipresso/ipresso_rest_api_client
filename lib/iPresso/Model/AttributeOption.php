<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

/**
 * Class AttributeOption
 * @package iPresso\Model
 */
class AttributeOption
{

    private string $key;

    private string $value;

    public function __construct(string $key, string $value)
    {
        $this->key = $key;
        $this->value = $value;
    }


    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): static
    {
        $this->key = $key;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): AttributeOption
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getOption(): array
    {
        if (!$this->value ||
            !$this->key) {
            throw new ApiException('Attribute option value and key missing');
        }

        return ['key' => $this->key, 'value'=> $this->value];
    }
}
