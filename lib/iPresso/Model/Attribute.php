<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

class Attribute
{
    const VAR_KEY = 'key';
    const VAR_NAME = 'name';
    const VAR_TYPE = 'type';
    const VAR_OPTION = 'option';

    /**
     * ATTRIBUTE TYPES
     */
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_DATE = 'date';
    const TYPE_DATE_TIME = 'datetime';
    const TYPE_INTEGER = 'integer';
    const TYPE_MULTI_SELECT = 'multiselect';
    const TYPE_SELECT = 'select';
    const TYPE_STRING = 'string';
    const TYPE_TEXT = 'text';
    const TYPE_TIME = 'time';

    /**
     * Available attribute types
     */
    public static array $attributeTypes = [
        self::TYPE_CHECKBOX,
        self::TYPE_DATE,
        self::TYPE_DATE_TIME,
        self::TYPE_INTEGER,
        self::TYPE_MULTI_SELECT,
        self::TYPE_SELECT,
        self::TYPE_STRING,
        self::TYPE_TEXT,
        self::TYPE_TIME
    ];

    public array $attribute = [];

    private string $key;

    private string $name;

    private string $type;

    private array $option = [];

    public function __construct(string $key, string $name, string $type)
    {
        $this->key = $key;
        $this->name = $name;
        $this->type = $type;
    }


    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): static
    {
        $this->key = $key;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Attribute
    {
        $this->name = $name;
        return $this;
    }

    public function addOption(string $key, string $value): Attribute
    {
        $this->option[$key] = $value;
        return $this;
    }

    public function getOption(): array
    {
        return $this->option;
    }

    public function setOption(array $option): Attribute
    {
        $this->option = $option;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getAttribute(bool $edit = false): array
    {
        if (!$edit && empty($this->name)) {
            throw new ApiException('Wrong attribute ' . self::VAR_NAME);
        }

        $this->attribute[self::VAR_NAME] = $this->name;

        if (empty($this->key)) {
            throw new ApiException('Wrong attribute ' . self::VAR_KEY);
        }

        $this->attribute[self::VAR_KEY] = $this->key;

        if (!$edit && !in_array($this->type, self::$attributeTypes)) {
            throw new ApiException('Wrong attribute ' . self::VAR_TYPE);
        }

        if (!$edit) {
            $this->attribute[self::VAR_TYPE] = $this->type;
        }

        if (!empty($this->option)) {
            $this->attribute[self::VAR_OPTION] = $this->option;
        }

        return $this->attribute;
    }


}
