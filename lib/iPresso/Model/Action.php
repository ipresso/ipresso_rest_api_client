<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

/**
 * Class Action
 * @package iPresso\Model
 */
class Action
{
    const VAR_KEY = 'key';
    const VAR_NAME = 'name';
    const VAR_PARAMETER = 'parameter';
    const VAR_TYPE = 'type';

    /**
     * ACTION TYPES
     */
    const TYPE_DECIMAL = 'decimal';
    const TYPE_DICTIONARY = 'dictionary';
    const TYPE_INTEGER = 'integer';
    const TYPE_STRING = 'string';
    const TYPE_DATETIME = 'datetime';
    const TYPE_BOOL = 'bool';
    const TYPE_MULTI = 'multi';
    const TYPE_IP = 'ip';

    public array $action = [];

    private string $key;

    private string $name;

    /**
     * @var string[]
     */
    private array $parameter = [];

    private static array $parameterTypes = [
        self::TYPE_DECIMAL,
        self::TYPE_DICTIONARY,
        self::TYPE_INTEGER,
        self::TYPE_STRING,
        self::TYPE_DATETIME,
        self::TYPE_BOOL,
        self::TYPE_MULTI,
        self::TYPE_IP,
    ];

    public function __construct(string $key, string $name)
    {
        $this->key = $key;
        $this->name = $name;
    }

    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(array $parameter): Action
    {
        $this->parameter = $parameter;
        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): Action
    {
        $this->key = $key;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Action
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function addParameter(string $name, string $key, string $type, array $options = []): static
    {
        $param = [];
        $param[self::VAR_NAME] = $name;
        $param[self::VAR_KEY] = $key;

        if (!in_array($type, self::$parameterTypes)) {
            throw new ApiException('Wrong parameter type.');
        }

        $param[self::VAR_TYPE] = $type;

        if (!empty($options)) {
            $param[self::VAR_PARAMETER] = $options;
        }

        $this->parameter[] = $param;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getAction(): array
    {
        if (empty($this->name)) {
            throw new ApiException('Wrong action name.');
        }

        $this->action[self::VAR_NAME] = $this->name;

        if (empty($this->key)) {
            throw new ApiException('Wrong action key.');
        }

        $this->action[self::VAR_KEY] = $this->key;

        if (!empty($this->parameter)) {
            $this->action[self::VAR_PARAMETER] = $this->parameter;
        }

        return $this->action;
    }
}
