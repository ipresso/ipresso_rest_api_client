<?php
declare(strict_types=1);

namespace iPresso\Model;

class NpsAnswer
{
    private int $score;
    private ?string $answer;
    private ?int $idContact;
    private ?string $idContactAnonymous;

    public function __construct(
        int $score,
        ?string $answer = null,
        ?int $idContact = null,
        ?string $idContactAnonymous = null
    )
    {
        $this->score = $score;
        $this->answer = $answer;
        $this->idContact = $idContact;
        $this->idContactAnonymous = $idContactAnonymous;
    }


    public function getScore(): int
    {
        return $this->score;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function getIdContact(): ?int
    {
        return $this->idContact;
    }

    public function getIdContactAnonymous(): ?string
    {
        return $this->idContactAnonymous;
    }

    public function setScore(int $score): NpsAnswer
    {
        $this->score = $score;
        return $this;
    }

    public function setAnswer(?string $answer): NpsAnswer
    {
        $this->answer = $answer;
        return $this;
    }

    public function setIdContact(?int $idContact): NpsAnswer
    {
        $this->idContact = $idContact;
        return $this;
    }

    public function setIdContactAnonymous(?string $idContactAnonymous): NpsAnswer
    {
        $this->idContactAnonymous = $idContactAnonymous;
        return $this;
    }
}
