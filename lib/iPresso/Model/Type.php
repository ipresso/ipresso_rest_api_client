<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

/**
 * Class Type
 * @package iPresso\Model
 */
class Type
{
    const VAR_ATTRIBUTE = 'attribute';
    const VAR_KEY = 'key';
    const VAR_NAME = 'name';
    const VAR_PARENT = 'parent';

    public array $type = [];

    private array $attribute = [];

    private string $key;

    private string $name;

    private ?string $parent;

    public function __construct(string $key, string $name, ?string $parent = null)
    {
        $this->key = $key;
        $this->name = $name;
        $this->parent = $parent;
    }


    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function setAttribute(array $attributeKey): static
    {
        $this->attribute = $attributeKey;
        return $this;
    }

    public function setKey(string $key): static
    {
        $this->key = $key;
        return $this;
    }

    public function addAttribute(string $attributeKey): static
    {
        $this->attribute[] = $attributeKey;
        return $this;
    }

    public function getParent(): string
    {
        return $this->parent;
    }

    public function setParent(string $parent): static
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getType(): array
    {
        if (empty($this->name)) {
            throw new ApiException('Wrong type name.');
        }

        $this->type[self::VAR_NAME] = $this->name;

        if (empty($this->key)) {
            throw new ApiException('Wrong type key.');
        }

        $this->type[self::VAR_KEY] = $this->key;

        if (!empty($this->attribute)) {
            $this->type[self::VAR_ATTRIBUTE] = $this->attribute;
        }

        if (!empty($this->parent)) {
            $this->type[self::VAR_PARENT] = $this->parent;
        }

        return $this->type;
    }

}
