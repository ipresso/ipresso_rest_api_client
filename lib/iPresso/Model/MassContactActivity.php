<?php
declare(strict_types=1);

namespace iPresso\Model;

/**
 * Class MassContactActivity
 * @package iPresso\Model
 */
class MassContactActivity
{
    private array $activities;

    public function __construct(array $activities)
    {
        $this->activities = $activities;
    }


    public function addContactActivity(int $idContact, ContactActivity $contactActivity): static
    {
        $this->activities[$idContact][] = $contactActivity;
        return $this;
    }

    public function getContactActivities(): array
    {
        return $this->activities;
    }
}
