<?php
declare(strict_types=1);

namespace iPresso\Model;

use Exception;
use iPresso\Exception\ApiException;

class ContactAction
{
    const VAR_DATE = 'date';
    const VAR_KEY = 'key';
    const VAR_PARAMETER = 'parameter';
    const VAR_VALUE = 'value';

    public array $contactAction = [];
    private \DateTime $date;
    private string $key;
    private array $parameter = [];

    public function __construct(string $key, \DateTime $date = new \DateTime())
    {
        $this->date = $date;
        $this->key = $key;
    }


    public function setKey(string $key): ContactAction
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function setDate(\DateTime $date): ContactAction
    {
        $this->date = $date;
        return $this;
    }

    public function addParameter(string $key, string $value): ContactAction
    {
        $this->parameter[$key] = $value;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getContactAction(): array
    {
        if (empty($this->key)) {
            throw new ApiException('Wrong action key.');
        }

        $this->contactAction[self::VAR_KEY] = $this->key;

        if (!empty($this->parameter)) {
            $this->contactAction[self::VAR_PARAMETER] = $this->parameter;
        }

        

        $this->contactAction[self::VAR_DATE] = $this->date;
        return $this->contactAction;
    }
}
