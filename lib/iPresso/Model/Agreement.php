<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

class Agreement
{
    const VAR_DESCRIPTION = 'description';
    const VAR_DM_STATUS = 'dmvisible';
    const VAR_NAME = 'name';

    const DIRECT_MARKETING_VISIBLE = 1;
    const DIRECT_MARKETING_NON_VISIBLE = 0;

    /**
     * @var int[]
     */
    public static array $dmStatusTypes = [
        self::DIRECT_MARKETING_NON_VISIBLE,
        self::DIRECT_MARKETING_VISIBLE
    ];

    /**
     * @var string[]
     */
    public array $agreement = [];

    /**
     * Parameter `AGREEMENT_NAME` should be replaced with the name of an agreement.
     */
    private string $name;

    /**
     * Parameter `AGREEMENT_DESCRIPTION` should be replaced with the content of an agreement.
     */
    private string $description = '';

    /**
     * Parameter `DM_VISIBLE_STATUS` defines whether agreement allows sending message: 0 if it doesn’t, 1 if it does.
     */
    private int $dmStatus = self::DIRECT_MARKETING_VISIBLE;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }


    public function getDmStatus(): int
    {
        return $this->dmStatus;
    }

    public function setDmStatus(int $dmStatus): static
    {
        $this->dmStatus = $dmStatus;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getAgreement(): array
    {
        if (empty($this->name)) {
            throw new ApiException('Wrong agreement name.');
        }

        $this->agreement[self::VAR_NAME] = $this->name;

        if (!empty($this->description)) {
            $this->agreement[self::VAR_DESCRIPTION] = $this->description;
        }

        if (!in_array($this->dmStatus, self::$dmStatusTypes)) {
            throw new ApiException('Wrong direct marketing visible status.');
        }

        $this->agreement[self::VAR_DM_STATUS] = $this->dmStatus;

        return $this->agreement;
    }
}
