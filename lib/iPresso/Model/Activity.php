<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

/**
 * Class Activity
 * @package iPresso\Model
 */
class Activity
{
    const VAR_KEY = 'key';
    const VAR_NAME = 'name';
    const VAR_PARAMETER = 'parameter';
    const VAR_TYPE = 'type';

    /**
     * ACTIVITY TYPES
     */
    const TYPE_DECIMAL = 'decimal';
    const TYPE_DICTIONARY = 'dictionary';
    const TYPE_INTEGER = 'integer';
    const TYPE_STRING = 'string';
    const TYPE_DATETIME = 'datetime';
    const TYPE_BOOL = 'bool';
    const TYPE_MULTI = 'multi';
    const TYPE_IP = 'ip';

    public array $activity = [];

    private string $key;

    private string $name;

    private array $parameter = [];

    private static array $parameterTypes = [
        self::TYPE_DECIMAL,
        self::TYPE_DICTIONARY,
        self::TYPE_INTEGER,
        self::TYPE_STRING,
        self::TYPE_DATETIME,
        self::TYPE_BOOL,
        self::TYPE_MULTI,
        self::TYPE_IP,
    ];

    public function __construct(string $key, string $name)
    {
        $this->key = $key;
        $this->name = $name;
    }

    public function getParameter(): array
    {
        return $this->parameter;
    }

    public function setParameter(array $parameter): static
    {
        $this->parameter = $parameter;
        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(mixed $key): static
    {
        $this->key = $key;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(mixed $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function addParameter(string $name, string $key, string $type, array $options = []): static
    {
        $param = [];
        $param[self::VAR_NAME] = $name;
        $param[self::VAR_KEY] = $key;

        if (!in_array($type, self::$parameterTypes)) {
            throw new ApiException('Wrong parameter type.');
        }

        $param[self::VAR_TYPE] = $type;

        if (!empty($options)) {
            $param[self::VAR_PARAMETER] = $options;
        }

        $this->parameter[] = $param;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getActivity(): array
    {
        if (empty($this->name)) {
            throw new ApiException('Wrong activity name.');
        }

        $this->activity[self::VAR_NAME] = $this->name;

        if (empty($this->key)) {
            throw new ApiException('Wrong activity key.');
        }

        $this->activity[self::VAR_KEY] = $this->key;

        if (!empty($this->parameter)) {
            $this->activity[self::VAR_PARAMETER] = $this->parameter;
        }

        return $this->activity;
    }
}
