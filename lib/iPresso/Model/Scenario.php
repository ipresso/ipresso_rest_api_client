<?php
declare(strict_types=1);

namespace iPresso\Model;

class Scenario
{

    /** @var string[] $contact contacts Ids */
    private array $contact;
    /** @var array<string,array> $contactData contact id => contact data */
    private array $contactData;
    /** @var string|array additional scenario data */
    private string|array $data;

    public function __construct(array $contact = [], array $contactData = [], array|string $data = [])
    {
        $this->contact = $contact;
        $this->contactData = $contactData;
        $this->data = $data;
    }


    public function addContact(string $idContact): Scenario
    {
        $this->contact[] = $idContact;
        return $this;
    }

    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(array $contact): Scenario
    {
        $this->contact = $contact;
        return $this;
    }

    public function addContactData(string $idContact, array $data): Scenario
    {
        $this->contactData[$idContact] = $data;
        return $this;
    }

    public function setContactData(array $contactData): Scenario
    {
        $this->contactData = $contactData;
        return $this;
    }

    public function getContactData(): array
    {
        return $this->contactData;
    }

    public function setData(array|string $data): Scenario
    {
        $this->data = $data;
        return $this;
    }

    public function getData(): array|string
    {
        return $this->data;
    }
}
