<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

class ContactActivity
{
    const VAR_DATE = 'date';
    const VAR_KEY = 'key';
    const VAR_PARAMETER = 'parameter';
    const VAR_VALUE = 'value';

    public array $contactActivity = [];
    private \DateTime|string $date;
    private string $key;
    private array $parameter = [];

    public function __construct(string $key, \DateTime $date = new \DateTime())
    {
        $this->date = $date;
        $this->key = $key;
    }


    public function setKey(string $key): ContactActivity
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function setDate(string $date): ContactActivity
    {
        try {
            $date = (new \DateTime($date))->format('Y-m-d H:i:s');
        } catch (\Exception $e) {
            throw new ApiException($e->getMessage());
        }
        $this->date = $date;
        return $this;
    }

    public function addParameter(string $key, mixed $value): ContactActivity
    {
        $this->parameter[$key] = $value;
        return $this;
    }


    /**
     * @throws ApiException
     */
    public function getContactActivity(): array
    {
        if (empty($this->key)) {
            throw new ApiException('Wrong activity key.');
        }

        $this->contactActivity[self::VAR_KEY] = $this->key;

        if (!empty($this->parameter)) {
            $this->contactActivity[self::VAR_PARAMETER] = $this->parameter;
        }

        if (empty($this->date)) {
            $this->date = date('Y-m-d H:i:s');
        }

        $this->contactActivity[self::VAR_DATE] = $this->date;
        return $this->contactActivity;
    }
}
