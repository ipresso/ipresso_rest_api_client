<?php

declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

class Contact
{
    const REGION = 'region';
    const VAR_AGREEMENT = 'agreement';
    const VAR_BUILDING_NUMBER = 'buildingNumber';
    const VAR_CATEGORY = 'category';
    const VAR_CITY = 'city';
    const VAR_COMPANY = 'company';
    const VAR_COUNTRY = 'country';
    const VAR_EMAIL = 'email';
    const VAR_FIRST_NAME = 'fname';
    const VAR_FIRST_NAME_PROPERTY = 'firstName';
    const VAR_FLAT_NUMBER = 'flatNumber';
    const VAR_ID_EXTERNAL = 'idExternal';
    const VAR_IP = 'ip';
    const VAR_LAST_NAME = 'lname';
    const VAR_LAST_NAME_PROPERTY = 'lastName';
    const VAR_MOBILE = 'mobile';
    const VAR_NAME = 'name';
    const VAR_ORIGIN = 'origin';
    const VAR_PHONE = 'phone';
    const VAR_POST_CODE = 'postCode';
    const VAR_STREET = 'street';
    const VAR_TAG = 'tag';
    const VAR_TYPE = 'type';
    const VAR_WORK_POSITION = 'workPosition';
    const VAR_WWW = 'www';
    const VAR_CONTACT_ACCESS = 'contactAccess';

    /**
     * iPresso API null variable
     */
    public static string $null = 'API_NULL';

    /**
     * @var Array<string,mixed>
     */
    private array $attribute = [];

    /**
     * Building number
     */
    private ?string $buildingNumber = '';

    /**
     * City
     */
    private ?string $city = '';

    /**
     * Name of company
     */
    private ?string $company = '';

    /**
     * Country
     */
    private ?string $country = '';

    /**
     * E-mail address
     * @required *
     */
    private string $email;

    /**
     * Flat number
     */
    private ?string $flatNumber = '';

    /**
     * Region
     */
    private ?string $region = '';

    /**
     * First name
     */
    private string $firstName = '';

    private ?int $idContact = null;

    /**
     * Last name
     * @required *
     */
    private string $lastName = '';

    /**
     * Mobile phone number
     * @required *
     */
    private string $mobile = '';

    /**
     * Own name
     * @required *
     */
    private string $name = '';

    /**
     * Landline phone number
     */
    private ?string $phone = '';

    /**
     * Post code
     */
    private ?string $postCode = '';

    /**
     * Street
     */
    private ?string $street = '';

    /**
     * Key of contact’s type
     */
    private ?string $type = '';

    /**
     * Associative array with a pair - agreement IDs => agreement status, where 1 = add agreement, 2 = delete agreement
     */
    private array $agreement = [];

    /**
     * Associative array with a pair - category ID => category status, where 1 = add category, 2 = delete category
     * @var array
     */
    private array $category = [];
    /**
     * Associative array with a pair - contact access ID => status, where 1 = add contact access, 2 = delete contact access
     * @var array
     */
    private array $contactAccess = [];

    /**
     * One-dimensional array containing tag
     * @var array
     */
    private array $tag = [];

    private ?string $origin = '';

    /**
     * IP Address
     */
    private ?string $ip = '';

    private ?string $www = '';

    private ?string $workPosition = '';

    private ?string $modifyDate = '';

    private ?string $idExternal = '';

    private array $properties = [
        self::VAR_TYPE => self::VAR_TYPE,
        self::VAR_FIRST_NAME => self::VAR_FIRST_NAME_PROPERTY,
        self::VAR_LAST_NAME => self::VAR_LAST_NAME_PROPERTY,
        self::VAR_NAME => self::VAR_NAME,
        self::VAR_EMAIL => self::VAR_EMAIL,
        self::VAR_PHONE => self::VAR_PHONE,
        self::VAR_MOBILE => self::VAR_MOBILE,
        self::VAR_COMPANY => self::VAR_COMPANY,
        self::VAR_CITY => self::VAR_CITY,
        self::VAR_POST_CODE => self::VAR_POST_CODE,
        self::VAR_FLAT_NUMBER => self::VAR_FLAT_NUMBER,
        self::VAR_BUILDING_NUMBER => self::VAR_BUILDING_NUMBER,
        self::REGION => self::REGION,
        self::VAR_COUNTRY => self::VAR_COUNTRY,
        self::VAR_CATEGORY => self::VAR_CATEGORY,
        self::VAR_TAG => self::VAR_TAG,
        self::VAR_AGREEMENT => self::VAR_AGREEMENT,
        self::VAR_ORIGIN => self::VAR_ORIGIN,
        self::VAR_IP => self::VAR_IP,
        self::VAR_WWW => self::VAR_WWW,
        self::VAR_STREET => self::VAR_STREET,
        self::VAR_WORK_POSITION => self::VAR_WORK_POSITION,
        self::VAR_ID_EXTERNAL => self::VAR_ID_EXTERNAL,
        self::VAR_CONTACT_ACCESS => self::VAR_CONTACT_ACCESS,
    ];

    public function __construct(
        string $email = '',
        string $name = '',
        string $mobile = '',
        string $lastName = ''
    ) {
        $this->email = $email;
        $this->name = $name;
        $this->mobile = $mobile;
        $this->lastName = $lastName;
    }


    public function setIdContact(int $idContact): Contact
    {
        $this->idContact = $idContact;
        return $this;
    }

    public function setBuildingNumber(?string $buildingNumber): Contact
    {
        $this->buildingNumber = $buildingNumber;
        return $this;
    }

    public function setCity(?string $city): Contact
    {
        $this->city = $city;
        return $this;
    }

    public function setCompany(?string $company): Contact
    {
        $this->company = $company;
        return $this;
    }

    public function setCountry(?string $country): Contact
    {
        $this->country = $country;
        return $this;
    }

    public function setEmail(string $email): Contact
    {
        $this->email = $email;
        return $this;
    }

    public function setFlatNumber(?string $flatNumber): Contact
    {
        $this->flatNumber = $flatNumber;
        return $this;
    }

    public function setFirstName(string $firstName): Contact
    {
        $this->firstName = $firstName;
        return $this;
    }

    public function setLastName(string $lastName): Contact
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function setMobile(string $mobile): Contact
    {
        $this->mobile = $mobile;
        return $this;
    }

    public function setName(string $name): Contact
    {
        $this->name = $name;
        return $this;
    }

    public function setPhone(?string $phone): Contact
    {
        $this->phone = $phone;
        return $this;
    }

    public function setPostCode(?string $postCode): Contact
    {
        $this->postCode = $postCode;
        return $this;
    }

    public function setStreet(?string $street): Contact
    {
        $this->street = $street;
        return $this;
    }

    public function setType(?string $type): Contact
    {
        $this->type = $type;
        return $this;
    }

    public function setAgreement(array $agreement): Contact
    {
        $this->agreement = $agreement;
        return $this;
    }

    public function addAgreement(int $idAgreement, int $status): Contact
    {
        $this->agreement[$idAgreement] = $status;
        return $this;
    }

    public function setCategory(array $category): Contact
    {
        $this->category = $category;
        return $this;
    }

    public function addCategory(int $idCategory, int $status): Contact
    {
        $this->category[$idCategory] = $status;
        return $this;
    }

    public function setTag(array $tag): Contact
    {
        $this->tag = $tag;
        return $this;
    }

    public function addTag(string $tag): Contact
    {
        $this->tag[] = $tag;
        return $this;
    }

    public function setAttribute(string $apiKey, mixed $value): Contact
    {
        $this->attribute[$apiKey] = is_bool($value) ? (int)$value : $value;
        return $this;
    }

    public function getAttribute(string $apiKey): bool|string
    {
        if (!isset($this->attribute[$apiKey])) {
            return false;
        }
        return $this->attribute[$apiKey];
    }

    public function getBuildingNumber(): ?string
    {
        return $this->buildingNumber;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getFlatNumber(): ?string
    {
        return $this->flatNumber;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): Contact
    {
        $this->region = $region;
        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getIdContact(): ?int
    {
        return $this->idContact;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getMobile(): string
    {
        return $this->mobile;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getPostCode(): ?string
    {
        return $this->postCode;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function getAgreement(): array
    {
        return $this->agreement;
    }

    public function getCategory(): array
    {
        return $this->category;
    }

    public function getTag(): array
    {
        return $this->tag;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    public function setOrigin(?string $origin): Contact
    {
        $this->origin = $origin;
        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): Contact
    {
        $this->ip = $ip;
        return $this;
    }

    public function getWww(): ?string
    {
        return $this->www;
    }

    public function setWww(?string $www): Contact
    {
        $this->www = $www;
        return $this;
    }

    public function getWorkPosition(): ?string
    {
        return $this->workPosition;
    }

    public function setWorkPosition(?string $workPosition): Contact
    {
        $this->workPosition = $workPosition;
        return $this;
    }

    public function getModifyDate(): ?string
    {
        return $this->modifyDate;
    }

    public function setModifyDate(?string $modifyDate): Contact
    {
        $this->modifyDate = $modifyDate;
        return $this;
    }

    public function getIdExternal(): ?string
    {
        return $this->idExternal;
    }

    public function setIdExternal(?string $idExternal): Contact
    {
        $this->idExternal = $idExternal;
        return $this;
    }

    public function getContactAccess(): array
    {
        return $this->contactAccess;
    }

    public function setContactAccess(array $contactAccess): void
    {
        $this->contactAccess = $contactAccess;
    }

    /**
     * @throws ApiException
     */
    public function getContact(bool $isUpdate = false): array
    {
        $contact = [];

        if (!$isUpdate && empty($this->email . $this->firstName . $this->lastName . $this->mobile)) {
            throw new ApiException('One of email|firstname|lastname|mobile is required');
        }

        if (
            ($this->email !== self::$null) &&
            (!empty($this->email) && !filter_var($this->email, FILTER_VALIDATE_EMAIL))
        ) {
            throw new ApiException('Wrong email address');
        }

        foreach ($this->properties as $key => $value) {
            if (!empty($this->$value)) {
                $contact[$key] = $this->$value;
            }
            if ($this->$value === null) {
                $contact[$key] = self::$null;
            }
        }

        if (!empty($this->attribute)) {
            foreach ($this->attribute as $apiKey => $value) {
                $contact[$apiKey] = $value;
            }
        }

        return $contact;
    }

}
