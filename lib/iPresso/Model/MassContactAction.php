<?php
declare(strict_types=1);

namespace iPresso\Model;

/**
 * Class MassContactAction
 * @package iPresso\Model
 */
class MassContactAction
{
    private array $actions;

    public function __construct(array $actions)
    {
        $this->actions = $actions;
    }


    public function addContactAction(int $idContact, ContactAction $contactAction): static
    {
        $this->actions[$idContact][] = $contactAction;
        return $this;
    }

    public function getContactActions(): array
    {
        return $this->actions;
    }
}
