<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

class Category
{
    const VAR_NAME = 'name';
    const VAR_PARENT_ID = 'parentId';

    public array $category = [];

    /**
     * Parameter `CATEGORY_NAME` should be replaced with the name of added category.
     */
    private string $name;

    /**
     * Parameter `PARENT_CATEGORY_ID` should be replaced with ID of parent category.
     * Not required
     */
    private ?int $parentId = null;

    public function __construct(string $name = '')
    {
        $this->name = $name;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Category
    {
        $this->name = $name;
        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function setParentId(?int $parentId): Category
    {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getCategory(): array
    {
        if (empty($this->name)) {
            throw new ApiException('Set category name first.');
        }

        $this->category[self::VAR_NAME] = $this->name;

        if (!empty($this->parentId)) {
            $this->category[self::VAR_PARENT_ID] = $this->parentId;
        }

        return $this->category;
    }
}
