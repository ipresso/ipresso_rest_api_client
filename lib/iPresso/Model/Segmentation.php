<?php
declare(strict_types=1);

namespace iPresso\Model;

use iPresso\Exception\ApiException;

class Segmentation
{

    const CONTACT_ORIGIN_ID = 1;
    const CONTACT_ORIGIN_EMAIL = 2;

    const VAR_CONTACT = 'contacts';
    const VAR_LIVE_TIME = 'live_time';
    const VAR_NAME = 'name';
    const VAR_ORIGIN = 'contact_origin';
    const VAR_TYPE = 'contact_type';
    const VAR_REFRESH = 'refresh';

    public array $segmentation = [];

    private array $contact = [];

    private ?int $contactOrigin;

    private ?string $contactType;

    private ?int $liveTime;

    private ?string $name;

    private ?bool $refresh;


    private static array $segmentationOrigin = [
        self::CONTACT_ORIGIN_ID,
        self::CONTACT_ORIGIN_EMAIL,
    ];

    public function __construct(
        ?int $liveTime = null,
        ?string $name = null,
        ?int $contactOrigin = null,
        ?string $contactType= null,
        ?bool $refresh = null
    )
    {
        $this->contactOrigin = $contactOrigin;
        $this->contactType = $contactType;
        $this->liveTime = $liveTime;
        $this->name = $name;
        $this->refresh = $refresh;
    }

    public function getContact(): array
    {
        return $this->contact;
    }

    public function setContact(array $contact): static
    {
        $this->contact = $contact;
        return $this;
    }

    public function addContact(int $idContact): static
    {
        $this->contact[] = $idContact;
        return $this;
    }

    public function getContactOrigin(): ?int
    {
        return $this->contactOrigin;
    }

    public function setContactOrigin(int $contactOrigin): static
    {
        $this->contactOrigin = $contactOrigin;
        return $this;
    }

    public function getLiveTime(): ?int
    {
        return $this->liveTime;
    }

    public function setLiveTime(int $liveTime): static
    {
        $this->liveTime = $liveTime;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getContactType(): ?string
    {
        return $this->contactType;
    }

    public function setContactType(string $contactType): static
    {
        $this->contactType = $contactType;
        return $this;
    }

    public function isRefresh(): ?bool
    {
        return $this->refresh;
    }

    public function setRefresh(bool $refresh): static
    {
        $this->refresh = $refresh;
        return $this;
    }

    /**
     * @throws ApiException
     */
    public function getSegmentation(): array
    {
        if (!in_array($this->contactOrigin, self::$segmentationOrigin)) {
            throw new ApiException('Wrong ' . self::VAR_ORIGIN);
        }

        if (empty($this->name)) {
            throw new ApiException('Wrong segmentation ' . self::VAR_NAME);
        }

        $this->segmentation[self::VAR_NAME] = $this->name;

        if (empty($this->liveTime)) {
            throw new ApiException('Wrong segmentation ' . self::VAR_LIVE_TIME);
        }

        $this->segmentation[self::VAR_LIVE_TIME] = $this->liveTime;

        if (!empty($this->contact)) {
            $this->segmentation[self::VAR_CONTACT] = $this->contact;
        }

        if (!empty($this->contactOrigin)) {
            $this->segmentation[self::VAR_ORIGIN] = $this->contactOrigin;
        }

        if (!empty($this->contactType)) {
            $this->segmentation[self::VAR_TYPE] = $this->contactType;
        }

        return $this->segmentation;
    }

    /**
     * @throws ApiException
     */
    public function getSegmentationContact(): array
    {
        if (empty($this->contact)) {
            throw new ApiException('No contacts in segmentation');
        }
        $this->segmentation[self::VAR_CONTACT] = $this->contact;

        if (empty($this->contactOrigin)) {
            throw new ApiException('Wrong segmentation ' . self::VAR_ORIGIN);
        }
        $this->segmentation[self::VAR_ORIGIN] = $this->contactOrigin;

        if (!empty($this->contactType)) {
            $this->segmentation[self::VAR_TYPE] = $this->contactType;
        }

        if ($this->refresh) {
            $this->segmentation[self::VAR_REFRESH] = $this->refresh;
        }

        return $this->segmentation;
    }
}
