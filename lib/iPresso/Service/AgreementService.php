<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Agreement;

/**
 * Class AgreementService
 * @package iPresso\Service
 */
class AgreementService
{
    private Service $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Add new agreements
     * @throws ApiException
     */
    public function add(Agreement $agreement): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('agreement')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($agreement->getAgreement())
            ->request();
    }

    /**
     * Get all available agreements
     * @throws ApiException
     */
    public function get(): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('agreement')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Edit agreement
     * @throws ApiException
     */
    public function edit(int $idAgreement, Agreement $agreement): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('agreement/' . $idAgreement)
            ->setRequestType(Service::REQUEST_METHOD_PUT)
            ->setPostData(['agreement' => $agreement->getAgreement()])
            ->request();
    }

    /**
     * Delete agreement
     * @throws ApiException
     */
    public function delete(int $idAgreement): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('agreement/' . $idAgreement)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * Add contacts to agreement
     * @throws ApiException
     */
    public function addContact(int $idAgreement, array $contactIds): Response|bool
    {
        if (empty($contactIds)) {
            throw new ApiException('Set idContacts array first.');
        }
        $data = [];

        $data['contact'] = $contactIds;
        return $this
            ->service
            ->setRequestPath('agreement/' . $idAgreement . '/contact')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Get contacts assigned to an agreement
     * @throws ApiException
     */
    public function getContact(int $idAgreement, ?int $page = null): Response|bool
    {
        if ($page > 0) {
            $page = '?page=' . $page;
        }

        return $this
            ->service
            ->setRequestPath('agreement/' . $idAgreement . '/contact' . $page)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Get contacts assigned to an agreement that are assigned to group
     * @throws ApiException
     */
    public function getAgreementsByGroup(string $groupApiKey): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('agreement/group/' . $groupApiKey)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Delete contact’s agreement
     * @throws ApiException
     */
    public function deleteContact(int $idAgreement, string $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('agreement/' . $idAgreement . '/contact/' . $idContact)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }
}
