<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Activity;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ActivityService
 * @package iPresso\Service
 */
class ActivityService
{

    private Service $service;
    private Serializer $serializer;

    public function __construct(Service $service, Serializer $serializer)
    {
        $this->service = $service;
        $this->serializer = $serializer;
    }

    /**
     * Add new activity
     * @throws ApiException
     */
    public function add(Activity $activity): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('activity')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($activity->getActivity())
            ->request();
    }

    /**
     * @throws ExceptionInterface
     * @throws ApiException
     */
    public function edit(string $activityKey, Activity $activity): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('activity/' . $activityKey)
            ->setRequestType(Service::REQUEST_METHOD_PUT)
            ->setPostData($activity->getActivity())
            ->request();
    }

    /**
     * @throws ApiException
     */
    public function delete(string $activityKey): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('activity/' . $activityKey)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * Get available activities
     * @throws ApiException
     */
    public function get(): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('activity')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * @throws ApiException
     */
    public function addContact(
        string $activityKey,
        array $contactIds,
        array $parameters = [],
        \DateTime $dateTime = new \DateTime()
    ): Response|bool
    {
        if (empty($contactIds)) {
            throw new ApiException('Set idContacts array first.');
        }

        $data = [
            'contact' => $contactIds,
            'parameter' => $parameters,
            'date' => $dateTime->format('Y-m-d H:i:s')
        ];
        return $this
            ->service
            ->setRequestPath('activity/' . $activityKey . '/contact')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }
}
