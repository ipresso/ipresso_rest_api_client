<?php
declare(strict_types=1);

namespace iPresso\Service;

use Closure;
use iPresso\Exception\ApiException;

/**
 * Class Service
 * @package iPresso\Service
 */
class Service
{
    const API_VER = 2;
    const REQUEST_METHOD_GET = 'GET';
    const REQUEST_METHOD_POST = 'POST';
    const REQUEST_METHOD_PUT = 'PUT';
    const REQUEST_METHOD_DELETE = 'DELETE';

    const HEADER_EXTERNAL_KEY = 'ipresso-external-key:';

    private \CurlHandle $curlHandler;

    private string $customerKey = '';

    private array $customHeaders = [];

    private bool $debug = false;

    private array $headers = [];
    private int $iteration = 0;

    private string $login = '';

    private string $password = '';

    private string $postData = '';

    private string $requestPath = '';

    /**
     * @var string
     */
    private string $requestType = self::REQUEST_METHOD_GET;

    private string $token = '';
    /**
     * jako argument tego callbacka będzie string $token
     */
    private Closure $tokenCallBack;

    private string $url = '';

    /**
     * @var integer
     */
    private int $version = self::API_VER;

    public function __construct()
    {
        $this->curlInit();
    }

    public function setLogin(string $login): Service
    {
        $this->login = $login;
        return $this;
    }

    public function setPassword(string $password): Service
    {
        $this->password = $password;
        return $this;
    }

    public function setToken(string $token): Service
    {
        $this->token = $token;
        return $this;
    }

    public function setUrl(string $url): Service
    {
        $this->url = $url . '/api/' . $this->version . '/';
        return $this;
    }

    public function setCustomerKey(string $customerKey): Service
    {
        $this->customerKey = $customerKey;
        return $this;
    }

    public function setRequestType(string $requestType): Service
    {
        $this->requestType = $requestType;
        return $this;
    }

    public function setRequestPath(string $requestPath): Service
    {
        $this->requestPath = $requestPath;
        return $this;
    }

    public function setPostData(array|string $postData): Service
    {
        if (is_array($postData) && !empty($postData)) {
            $this->postData = http_build_query($postData);
        }
        if (is_string($postData)) {
            $this->postData = $postData;
        }
        return $this;
    }

    public function debug(): Service
    {
        $this->debug = true;
        return $this;
    }

    public function setVersion(int $version): Service
    {
        $this->version = $version;
        return $this;
    }

    public function addCustomHeader(string $customHeader): Service
    {
        $this->customHeaders[] = $customHeader;
        return $this;
    }

    public function getTokenCallBack(): Closure
    {
        return $this->tokenCallBack;
    }

    public function setTokenCallBack(Closure $tokenCallBack): Service
    {
        $this->tokenCallBack = $tokenCallBack;
        return $this;
    }

    /**
     * @throws ApiException
     */
    private function tokenValidate(bool $getToken = false): static
    {
        if (!$this->token && !$getToken) {
            throw new ApiException('Set token first.');
        }

        if (!$this->login) {
            throw new ApiException('Set login first.');
        }

        if (!$this->password) {
            throw new ApiException('Set password first.');
        }

        if (!$this->url) {
            throw new ApiException('Set url first.');
        }

        if (!$this->customerKey) {
            throw new ApiException('Set customerKey first.');
        }

        return $this;
    }


    /**
     * @throws ApiException
     */
    public function request(): Response|bool
    {
        $this->tokenValidate();
        $this->type();
        $this->requestHeaders();
        $response2 = $this->exec($this->url . $this->requestPath);
        $response = Response::getInstance($response2);
        if ($response->code == Response::STATUS_FORBIDDEN) {
            if (!isset($response->errorCode) && $this->iteration < 5 && $this->getToken()) {
                $this->iteration++;
                return $this->request();
            }
        } else {
            $this->iteration = 0;
            return $response;
        }
        $this->iteration = 0;
        return false;
    }


    private function type(): static
{
        switch ($this->requestType) {
            case self::REQUEST_METHOD_DELETE:
                curl_setopt($this->curlHandler, CURLOPT_CUSTOMREQUEST, self::REQUEST_METHOD_DELETE);
                break;
            case self::REQUEST_METHOD_POST:
                curl_setopt($this->curlHandler, CURLOPT_POST, true);
                curl_setopt($this->curlHandler, CURLOPT_POSTFIELDS, $this->postData);
                curl_setopt($this->curlHandler, CURLOPT_CUSTOMREQUEST, self::REQUEST_METHOD_POST);
                break;
            case self::REQUEST_METHOD_PUT:
                curl_setopt($this->curlHandler, CURLOPT_CUSTOMREQUEST, self::REQUEST_METHOD_PUT);
                curl_setopt($this->curlHandler, CURLOPT_POSTFIELDS, $this->postData);
                break;
            case self::REQUEST_METHOD_GET:
                curl_setopt($this->curlHandler, CURLOPT_CUSTOMREQUEST, self::REQUEST_METHOD_GET);
                break;
            default:
                break;
        }
        return $this;
    }

    /**
     * Use to get session token
     * @throws ApiException
     */
    public function getToken(bool $fullResponse = false): Response|bool|string
    {
        $this->tokenValidate(true);
        $this->headers();
        $this->auth();
        $response = Response::getInstance($this->exec($this->url . 'auth/' . $this->customerKey));

        if ($fullResponse) {
            return $response;
        }

        if (200 == $response->code) {

            if (!empty($this->tokenCallBack)) {
                call_user_func($this->tokenCallBack, $response->data);
            }

            $this->token = $response->data;
            return $this->token;
        }
        return false;
    }

    /**
     * @throws ApiException
     */
    private function exec(string $url): ?array
    {
        curl_setopt($this->curlHandler, CURLOPT_URL, $url);
        curl_setopt($this->curlHandler, CURLOPT_HTTPHEADER, array_merge($this->headers, $this->customHeaders));
        $exec = curl_exec($this->curlHandler);

        if ($this->debug) {
            print_r($exec);
        }

        if ($exec === false) {
            throw new ApiException(curl_error($this->curlHandler));
        }

        return json_decode($exec, true);
    }

    private function headers(): static
    {
        $this->headers = array();
        $this->headers[] = 'ACCEPT: text/json';
        $this->headers[] = 'USER_AGENT: iPresso';
        return $this;
    }

    private function auth(): static
    {
        curl_setopt($this->curlHandler, CURLOPT_USERPWD, $this->login . ':' . $this->password);
        return $this;
    }

    private function requestHeaders(): static
    {
        $this->headers();
        $this->headers[] = 'IPRESSO_TOKEN: ' . $this->token;
        return $this;
    }

    private function curlInit(): void
    {
        $this->curlHandler = curl_init();
        curl_setopt($this->curlHandler, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($this->curlHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curlHandler, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curlHandler, CURLOPT_SSL_VERIFYHOST, false);
    }

    public static function dump(bool $die, mixed $variable, bool $desc = false, bool $noHtml = false): void
    {
        if (is_string($variable)) {
            $variable = str_replace("<_new_line_>", "<BR>", $variable);
        }

        if ($noHtml) {
            echo "\n";
        } else {
            echo "<pre>";
        }

        if ($desc) {
            echo $desc . ": ";
        }

        print_r($variable);

        if ($noHtml) {
            echo "";
        } else {
            echo "</pre>";
        }

        if ($die) {
            die();
        }
    }
}
