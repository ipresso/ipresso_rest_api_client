<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;

/**
 * Class OriginService
 * @package iPresso\Service
 */
class OriginService
{
    private Service $service;
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get all contact origins
     * @throws ApiException
     */
    public function get(): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('origin')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Get contacts from a given origin
     * @throws ApiException
     */
    public function getContact(int $idOrigin, ?int $page = null): Response|bool
    {
        if ($page > 0) {
            $page = '?page=' . $page;
        }

        return $this
            ->service
            ->setRequestPath('origin/' . $idOrigin . '/contact' . $page)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }
}
