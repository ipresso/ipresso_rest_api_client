<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\ContactActivity;

/**
 * Class ContactAnonymousService
 * @package iPresso\Service
 */
class ContactAnonymousService
{

    private Service $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Adding activity to a anonymous contact
     * @see https://apidocpl.ipresso.com/
     * @throws ApiException
     */
    public function addActivity(int $idContactAnonymous, ContactActivity $contactActivity): Response|bool
    {
        $data = [];
        $data['activity'][] = $contactActivity->getContactActivity();
        return $this
            ->service
            ->setRequestPath('anonymous/' . $idContactAnonymous . '/activity')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Adding activities to an anonymous contact
     * @see https://apidocpl.ipresso.com/
     * @throws ApiException
     */
    public function addActivities(int $idContactAnonymous, array $contactActivities): Response|bool
    {
        $data = [];
        foreach ($contactActivities as $contactActivity) {
            if ($contactActivity instanceof ContactActivity && !empty($contactActivity)) {
                $data['activity'][] = $contactActivity->getContactActivity();
            }
        }

        return $this
            ->service
            ->setRequestPath('anonymous/' . $idContactAnonymous . '/activity')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }


}
