<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Tag;

/**
 * Class TagService
 * @package iPresso\Service
 */
class TagService
{

    private Service $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get tags
     * @throws ApiException
     */
    public function get(?int $idTag = null): Response|bool
    {
        if ($idTag) {
            $idTag = '/' . $idTag;
        }

        return $this
            ->service
            ->setRequestPath('tag' . $idTag)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Add new tag
     * @throws ApiException
     */
    public function add(Tag $tag): Response|bool
    {
        $data = [];
        $data['name'] = $tag;
        return $this
            ->service
            ->setRequestPath('tag')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Edit selected tag
     * @throws ApiException
     */
    public function edit(int $idTag, Tag $tag): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('tag/' . $idTag)
            ->setRequestType(Service::REQUEST_METHOD_PUT)
            ->setPostData(['tag' => $tag->getTag()])
            ->request();
    }

    /**
     * Delete tag
     * @throws ApiException
     */
    public function delete(int $idTag): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('tag/' . $idTag)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * Add new contacts to a tag
     * @throws ApiException
     */
    public function addContact(int $idTag, array $contactIds): Response|bool
    {
        if (empty($contactIds)) {
            throw new ApiException('Set idContacts array first.');
        }

        $data = [];
        $data['contact'] = $contactIds;
        return $this
            ->service
            ->setRequestPath('tag/' . $idTag . '/contact')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Get all contacts in tag
     * @throws ApiException
     */
    public function getContact(int $idTag, ?int $page = null): Response|bool
    {
        if ($page) {
            $page = '?page=' . $page;
        }

        return $this
            ->service
            ->setRequestPath('tag/' . $idTag . '/contact' . $page)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Delete contact in tag
     * @throws ApiException
     */
    public function deleteContact(int $idTag, int|string $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('tag/' . $idTag . '/contact/' . $idContact)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

}
