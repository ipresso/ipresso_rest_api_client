<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Action;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ActionService
 * @package iPresso\Service
 */
class ActionService
{
    private Service $service;

    private Serializer $serializer;

    public function __construct(Service $service, Serializer $serializer)
    {
        $this->service = $service;
        $this->serializer = $serializer;
    }

    /**
     * Add new actions
     * @throws ApiException
     */
    public function add(Action $action): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('action')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($action->getAction())
            ->request();
    }

    /**
     * Get available actions
     * @throws ApiException
     */
    public function get(): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('action')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * @throws ApiException
     */
    public function edit(string $actionKey, Action $action): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('action/' . $actionKey)
            ->setRequestType(Service::REQUEST_METHOD_PUT)
            ->setPostData($action->getAction())
            ->request();
    }

    /**
     * @throws ApiException
     */
    public function delete(string $actionKey): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('action/' . $actionKey)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * @throws ApiException
     */
    public function addContact(
        string $actionKey,
        array $contactIds,
        array $parameters = [],
        \DateTime|bool $dateTime = false
    ): Response|bool
    {
        if (empty($contactIds)) {
            throw new ApiException('Set idContacts array first.');
        }

        $data = [
            'contact' => $contactIds,
            'parameter' => $parameters,
            'date' => $dateTime instanceof \DateTime ? $dateTime->format('Y-m-d H:i:s') : date('Y-m-d H:i:s'),
        ];
        return $this
            ->service
            ->setRequestPath('action/' . $actionKey . '/contact')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }
}
