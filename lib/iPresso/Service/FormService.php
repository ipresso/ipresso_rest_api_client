<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Form;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

class FormService
{
    private Service $service;

    private Serializer $serializer;

    public function __construct(Service $service, Serializer $serializer)
    {
        $this->service = $service;
        $this->serializer = $serializer;
    }

    /**
     * Add form filling
     * @throws ApiException
     * @throws ExceptionInterface
     */
    public function fillForm(string $formApiKey, Form $form, bool $testMode = false): Response|bool
    {
        if (empty($formApiKey)) {
            throw new ApiException('Form api key is required.');
        }
        $form->validate();
        $query = '';
        if($testMode){
            $query = "?". http_build_query(['ipressoTestForm' => true]);
        }

        return $this
            ->service
            ->setRequestPath("form/{$formApiKey}/fill{$query}")
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData(json_encode($form))
            ->addCustomHeader("Content-Type: application/json")
            ->request();
    }
}
