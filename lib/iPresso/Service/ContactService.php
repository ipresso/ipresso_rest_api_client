<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Contact;
use iPresso\Model\ContactAction;
use iPresso\Model\ContactActivity;
use iPresso\Model\MassContactAction;
use iPresso\Model\MassContactActivity;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ContactService
 * @package iPresso\Service
 */
class ContactService
{

    private Service $service;

    private Serializer $serializer;

    const CONTACT = "contact";

    public function __construct(Service $service, Serializer $serializer)
    {
        $this->service = $service;
        $this->serializer = $serializer;
    }

    /**
     * Adding new contact
     * @see https://apidoc.ipresso.com/#64917262-7ef4-c3d2-ba2c-cb572b223c7e
     * @throws ApiException
     */
    public function add(array|Contact $contact): Response|bool
    {
        $postData = [];

        if (is_array($contact)) {
            foreach ($contact as $c) {
                $postData['contact'][] = $c->getContact();
            }
        } else {
            $postData['contact'][] = $contact->getContact();
        }

        return $this
            ->service
            ->setRequestPath(self::CONTACT)
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($postData)
            ->request();
    }

    /**
     * Edition of a contact with a given ID number
     * @see https://apidoc.ipresso.com/#202c606c-dbfe-57cd-2c2c-9749ada93e63
     * @throws ApiException
     */
    public function edit(int $idContact, Contact $contact): Response|bool
    {
        if (!$idContact) {
            $idContact = $contact->getIdContact();
        }

        if (!$idContact) {
            throw new ApiException('Contact id missing.');
        }

        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact)
            ->setRequestType(Service::REQUEST_METHOD_PUT)
            ->setPostData([self::CONTACT => $contact->getContact(true)])
            ->request();
    }

    /**
     * Delete contact
     * @see https://apidoc.ipresso.com/#154693f5-1d95-941c-5998-b8c15a3f9963
     * @throws ApiException
     */
    public function delete(int $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * Collect contact’s data with a given ID number
     * @see https://apidoc.ipresso.com/#e3b7f9c4-54f7-28c3-b0f9-74d5bdeaf5d5
     * @throws ApiException
     */
    public function get(int $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Adding tags to contacts with a given ID
     * @see https://apidoc.ipresso.com/#d7d75e02-534c-883a-3a49-59683d9da7e1
     * @throws ApiException
     */
    public function addTag(int $idContact, string $tagString): Response|bool
    {
        $data = [];
        $data['tag'] = [$tagString];
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/tag')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Collecting tags for a contact
     * @see https://apidoc.ipresso.com/#2b80736c-0668-ad28-aae3-c2175bcd385c
     * @throws ApiException
     */
    public function getTag(int $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/tag')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Deleting contact’s tag
     * @see https://apidoc.ipresso.com/#792dec32-4d65-6e0a-03f9-88b20bafb0ab
     * @throws ApiException
     */
    public function deleteTag(int $idContact, int $idTag): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/tag/' . $idTag)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * Adding category to a contact
     * @see https://apidoc.ipresso.com/#38cbb27d-9954-4ce1-84d5-7e14535f51ee
     * @throws ApiException
     */
    public function addCategory(int $idContact, array $categoryIds): Response|bool
    {
        $data = [];
        $data['category'] = $categoryIds;
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/category')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Get category for a contact
     * @see https://apidoc.ipresso.com/#5d503c3f-a60a-e6c4-9310-3b9c9d4561fd
     * @throws ApiException
     */
    public function getCategory(int $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/category')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Deleting contact’s category
     * @see https://apidoc.ipresso.com/#615c65ab-ff9c-d1df-ba5e-a56506311e0c
     * @throws ApiException
     */
    public function deleteCategory(int $idContact, int $idCategory): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/category/' . $idCategory)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * Get integration of the contact
     * @see https://apidoc.ipresso.com/#81108b5b-cfbb-997d-6b94-5fabbbac65f8
     * @throws ApiException
     */
    public function getIntegration(int $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/integration')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Adding agreements to contacts
     * Parameter `ID_AGREEMENT` should be replaced with ID number of the agreement.
     * Then add status of the agreement. In the case of acceptance the status equals 1.
     * Parameter `ID_AGREEMENT_STATUS` is agreement status,
     * in the case of activation of agreement enter number 1, in other cases enter 2.
     * @see https://apidoc.ipresso.com/#5834791e-08dc-7987-e600-695e0ef01696
     * [ID_AGREEMENT => ID_AGREEMENT_STATUS]
     * @throws ApiException
     */
    public function addAgreement(int $idContact, array $agreement): Response|bool
    {
        $data = [];
        $data['agreement'] = $agreement;
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/agreement')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Get contact’s agreements
     * @see https://apidoc.ipresso.com/#61c6acba-9a81-bc92-3f36-b00b903d83f3
     * @throws ApiException
     */
    public function getAgreement(int $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/agreement')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Delete agreement of a given ID from a contact of a given ID
     * @see https://apidoc.ipresso.com/#8922aaee-3bd9-346e-06e5-64a49b947a47
     * @throws ApiException
     */
    public function deleteAgreement(int $idContact, int $idAgreement): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/agreement/' . $idAgreement)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * Adding activity to a contact
     * @see https://apidoc.ipresso.com/#6e6ec20f-60c3-ab1b-cf1a-7355e7bcb5b0
     * @throws ApiException
     */
    public function addActivity(int $idContact, ContactActivity $contactActivity): Response|bool
    {
        $data = [];
        $data['activity'][] = $contactActivity->getContactActivity();
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/activity')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Adding activities to a contact
     * @see https://apidoc.ipresso.com/#6e6ec20f-60c3-ab1b-cf1a-7355e7bcb5b0
     * @throws ApiException
     */
    public function addActivities(int $idContact, array $contactActivities): Response|bool
    {
        $data = [];
        foreach ($contactActivities as $contactActivity) {
            if ($contactActivity instanceof ContactActivity) {
                $data['activity'][] = $contactActivity->getContactActivity();
            }
        }

        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/activity')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Get available activities
     * @see https://apidoc.ipresso.com/#6b572d86-82b7-6173-a02d-d5de1f8045dc
     * @throws ApiException
     */
    public function getActivity(int $idContact, ?int $page = null): Response|bool
    {
        if ($page) {
            $page = '?page=' . $page;
        }

        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/activity' . $page)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Add actions to contact
     * @see https://apidoc.ipresso.com/#239d36dc-bee0-b709-62ae-7f8888104f77
     * @throws ApiException
     */
    public function addAction(int $idContact, ContactAction $contactAction): Response|bool
    {
        $data = [];
        $data['action'][] = $contactAction->getContactAction();
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/action')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Get available actions
     * @see https://apidoc.ipresso.com/#a7e6069c-4fd5-4593-28f1-fe724d2f229c
     * @throws ApiException
     */
    public function getAction(int $idContact, ?int $page = null): Response|bool
    {
        if ($page) {
            $page = '?page=' . $page;
        }

        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/action' . $page)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Get contact type
     * @see https://apidoc.ipresso.com/#879b0763-a0b8-8e02-9b1b-82d6642800b8
     * @throws ApiException
     */
    public function getType(int $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/type')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Setting contact type
     * @see https://apidoc.ipresso.com/#89b33ee2-2f83-1c89-3948-dfd4c2d22eb5
     * @throws ApiException
     */
    public function setType(int $idContact, string $typeKey): Response|bool
    {
        $data = [];
        $data['type'] = $typeKey;
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/type')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Get connections between contacts
     * @see https://apidoc.ipresso.com/#410de216-761a-682d-4465-13e9eb15d67c
     * @throws ApiException
     */
    public function getConnection(string $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/connection')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Connect contacts
     * @see https://apidoc.ipresso.com/#c9b680f0-0646-53f0-5bbf-c879470f7183
     * @throws ApiException
     */
    public function setConnection(int $idContact, string $idContactToConnect): Response|bool
    {
        $data = [];
        $data['connection'] = [$idContactToConnect];
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/connection')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Delete connection between contacts
     * @see https://apidoc.ipresso.com/#54476e47-3fa7-a356-54f1-d6649a55942f
     * @throws ApiException
     */
    public function deleteConnection(int $idContact, int $idChild): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/connection/' . $idChild)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * Mass addition of activities to contacts
     * @see https://apidoc.ipresso.com/#93299f16-af1c-a285-f4bb-ec2b41545ee1
     * @throws ApiException|ExceptionInterface
     */
    public function addMassActivity(MassContactActivity $massContactActivity): Response|bool
    {
        $data = [];
        $data['contact'] = $this->serializer->normalize($massContactActivity->getContactActivities());
        return $this
            ->service
            ->setRequestPath('contact/activity')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Mass addition of actions to contacts
     * @see https://apidoc.ipresso.com/#e8f089ed-5c3a-3e5b-0389-3097b722a6d1
     * @throws ApiException|ExceptionInterface
     */
    public function addMassAction(MassContactAction $massContactAction): Response|bool
    {
        $data = [];
        $data['contact'] = $this->serializer->normalize($massContactAction);
        return $this
            ->service
            ->setRequestPath('contact/action')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Get contact's profile pages
     * @see https://apidoc.ipresso.com/#9d477a7d-ea96-754f-e8ca-92160cb916b5
     * @throws ApiException
     */
    public function getProfilePage(int $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/profilepage')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Get connections between contacts
     * @see https://apidoc.ipresso.com/#410de216-761a-682d-4465-13e9eb15d67c
     * @throws ApiException
     */
    public function getConnectionExtended(int $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/connection/extended')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Get contact’s agreements that are assigned to group
     * @see https://apidoc.ipresso.com/#e2566e5b-a7d2-4071-bbd5-baeab73e94e3
     * @throws ApiException
     */
    public function getAgreementsByGroup(int $idContact, string $groupApiKey): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CONTACT . '/' . $idContact . '/agreement/group/'.$groupApiKey)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }
}
