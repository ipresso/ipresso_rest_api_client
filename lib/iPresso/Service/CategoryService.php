<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Category;

/**
 * Class CategoryService
 * @package iPresso\Service
 */
class CategoryService
{
    private Service $service;

    const CATEGORY = "category";

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get category
     * @throws ApiException
     */
    public function get(?int $idCategory = null): Response|bool
    {
        if ($idCategory) {
            $idCategory = '/' . $idCategory;
        }

        return $this
            ->service
            ->setRequestPath(self::CATEGORY . $idCategory)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Add new category
     * @throws ApiException
     */
    public function add(Category $category): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CATEGORY)
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($category->getCategory())
            ->request();
    }

    /**
     * Edit selected category
     * @throws ApiException
     */
    public function edit(int $idCategory, Category $category): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CATEGORY . '/' . $idCategory)
            ->setRequestType(Service::REQUEST_METHOD_PUT)
            ->setPostData([self::CATEGORY => $category->getCategory()])
            ->request();
    }

    /**
     * Delete category
     * @throws ApiException
     */
    public function delete(int $idCategory): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CATEGORY . '/' . $idCategory)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * Add new contacts to categories
     * @throws ApiException
     */
    public function addContact(int $idCategory, array $contactIds): Response|bool
    {
        if (empty($contactIds)) {
            throw new ApiException('Set idContacts array first.');
        }

        $data = [];
        $data['contact'] = $contactIds;
        return $this
            ->service
            ->setRequestPath(self::CATEGORY. '/' . $idCategory . '/contact')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Remove contacts from category
     * @throws ApiException
     */
    public function removeContacts(int $idCategory, array $contactIds): Response|bool
    {
        if (empty($contactIds)) {
            throw new ApiException('Set idContacts array first.');
        }

        $data = [];
        $data['contact'] = $contactIds;
        return $this
            ->service
            ->setRequestPath(self::CATEGORY . '/' . $idCategory . '/contact-remove')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Get all contacts in category
     * @throws ApiException
     */
    public function getContact(int $idCategory, ?int $page = null): Response|bool
    {
        if ($page > 0) {
            $page = '?page=' . $page;
        }

        return $this
            ->service
            ->setRequestPath(self::CATEGORY . '/' . $idCategory . '/contact' . $page)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Delete contact in category
     * @throws ApiException
     */
    public function deleteContact(int $idCategory, string $idContact): Response|bool
    {
        return $this
            ->service
            ->setRequestPath(self::CATEGORY . '/' . $idCategory . '/contact/' . $idContact)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }
}
