<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Scenario;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;


class ScenarioService
{

    private Service $service;

    private Serializer $serializer;

    public function __construct(Service $service, Serializer $serializer)
    {
        $this->service = $service;
        $this->serializer = $serializer;
    }

    /**
     * Add contacts to scenario
     * @throws ApiException
     * @throws ExceptionInterface
     */
    public function addContacts(string $key, Scenario $scenario): Response|bool
    {
        if (!$key) {
            throw new ApiException('Scenario key is missing.');
        }

        return $this
            ->service
            ->setRequestPath('scenario/' . $key . '/contact')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData((array)$this->serializer->normalize($scenario))
            ->request();
    }

    /**
     * Start scenario
     * @throws ApiException
     * @throws ExceptionInterface
     */
    public function startScenario(string $key, Scenario $scenario = null): Response|bool
    {
        if (!$key) {
            throw new ApiException('Scenario key is missing.');
        }

        return $this
            ->service
            ->setRequestPath('scenario/' . $key . '/start')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData((array)$this->serializer->normalize($scenario))
            ->request();
    }

    /**
     * Hold scenario
     * @throws ApiException
     */
    public function holdScenario(string $key): Response|bool
    {
        if (!$key) {
            throw new ApiException('Scenario key is missing.');
        }

        return $this
            ->service
            ->setRequestPath('scenario/' . $key . '/hold')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->request();
    }
}
