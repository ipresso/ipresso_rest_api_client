<?php
declare(strict_types=1);

namespace iPresso\Service;


use iPresso\Exception\ApiException;

/**
 * Class WebsiteService
 * @package iPresso\Service
 */
class WebsiteService
{
    private Service $service;

    /**
     * WebsiteService constructor.
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get monitored websites
     * @throws ApiException
     */
    public function get(?int $idWww = null): Response|bool
    {
        if ($idWww) {
            $idWww = '/' . $idWww;
        }
        return $this
            ->service
            ->setRequestPath('www' . $idWww)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Add monitored website
     * @param array $jsApiMethods [method api key => 0|1]
     * @throws ApiException
     */
    public function add(string $url, array $jsApiMethods = []): Response|bool
    {
        if (!$url || !filter_var($url, FILTER_VALIDATE_URL)) {
            throw new ApiException('Set correct URL.');
        }

        foreach ($jsApiMethods as $methodKey => $isOn) {
            if (!is_string($methodKey)) {
                throw new ApiException('Js api method key must be string.');
            }
        }

        $data = [];
        $data['www']['url'] = $url;
        $data['www']['js_api_methods'] = $jsApiMethods;

        return $this
            ->service
            ->setRequestPath('www')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }

    /**
     * Delete monitored website
     * @throws ApiException
     */
    public function delete(int $idWww): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('www/' . $idWww)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

}
