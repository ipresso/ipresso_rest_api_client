<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Segmentation;

/**
 * Class SegmentationService
 * @package iPresso\Service
 */
class SegmentationService
{

    private Service $service;

    /**
     * SegmentationService constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * @throws ApiException
     */
    public function get(): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('segmentation')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * @see https://apidoc.ipresso.com/?version=latest#7e8e472f-2487-2a3a-0bf0-36c70a6d68a8
     * @throws ApiException
     */
    public function add(Segmentation $segmentation): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('segmentation')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($segmentation->getSegmentation())
            ->request();
    }

    /**
     * @throws ApiException
     */
    public function delete(int $idSegmentation): Response|bool
    {
        $idSegmentation = self::verifySegmentationId($idSegmentation);

        return $this
            ->service
            ->setRequestPath('segmentation/' . $idSegmentation)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * @throws ApiException
     */
    public function addContact(int $idSegmentation, Segmentation $segmentation): Response|bool
    {
        $idSegmentation = self::verifySegmentationId($idSegmentation);

        return $this
            ->service
            ->setRequestPath('segmentation/' . $idSegmentation . '/contact')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($segmentation->getSegmentationContact())
            ->request();
    }

    /**
     * Remove multiple contacts from existing segmentation
     * @param int $idSegmentation ID of the segmentation to remove contacts from
     * @param Segmentation $segmentation Object containing contacts to be removed from iPresso segmentation
     * @return Response|bool
     * @throws ApiException
     */
    public function removeContact(int $idSegmentation, Segmentation $segmentation): Response|bool
    {
        $idSegmentation = self::verifySegmentationId($idSegmentation);

        return $this
            ->service
            ->setRequestPath('segmentation/' . $idSegmentation . '/contact-remove')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($segmentation->getSegmentationContact())
            ->request();

    }

    /**
     * @throws ApiException
     */
    public function getContact(int $idSegmentation): Response|bool
    {
        $idSegmentation = self::verifySegmentationId($idSegmentation);

        return $this
            ->service
            ->setRequestPath('segmentation/' . $idSegmentation)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * @param $idSegmentation
     * @return int
     * @throws ApiException
     */
    private static function verifySegmentationId($idSegmentation): int
    {
        if (empty($idSegmentation)) {
            throw new ApiException('Please provide `$idSegmentation`');
        }
        if (!$idSegmentation = (int) $idSegmentation) {
            throw new ApiException('`$idSegmentation` needs to be coercible to `int`');
        }
        if ($idSegmentation <= 0) {
            throw new ApiException('`$idSegmentation` needs to be integer greater than 0');
        }
        return $idSegmentation;
    }
}
