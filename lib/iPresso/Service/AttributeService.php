<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Attribute;
use iPresso\Model\AttributeOption;
use iPresso\Model\CustomerAttribute;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Class AttributeService
 * @package iPresso\Service
 */
class AttributeService
{

    private Service $service;

    private Serializer $serializer;

    /**
     * AttributeService constructor.
     * @param Service $service
     * @param Serializer $serializer
     */
    public function __construct(Service $service, Serializer $serializer)
    {
        $this->service = $service;
        $this->serializer = $serializer;
    }

    /**
     * Add new attributes
     * @throws ApiException
     */
    public function add(Attribute $attribute): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('attribute')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($attribute->getAttribute())
            ->request();
    }

    /**
     * Get available attributes
     * @throws ApiException
     */
    public function get(): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('attribute')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Add new options to attribute
     * @throws ApiException
     */
    public function addOption(string $attributeKey, AttributeOption $option): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('attribute/' . $attributeKey . '/option')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData(['option' => [$option->getKey() => $option->getValue()]])
            ->request();
    }

    /**
     * Edit attribute option
     * @throws ApiException|ExceptionInterface
     */
    public function editOption(
        string $attributeKey,
        AttributeOption $option,
        ?string $optionKeyEdit = null
    ): Response|bool
    {

        if ($optionKeyEdit) {
            $optionAttributeKey = $optionKeyEdit;
        } else {
            $optionAttributeKey = $option->getKey();
        }

        if (!$optionAttributeKey) {
            throw new ApiException('Attribute option key missing');
        }

        return $this
            ->service
            ->setRequestPath('attribute/' . $attributeKey . '/option/' . $optionAttributeKey)
            ->setRequestType(Service::REQUEST_METHOD_PUT)
            ->setPostData(['option' => $option->getOption()])
            ->request();
    }


    /**
     * @throws ApiException
     */
    public function deleteOption(string $attributeKey, AttributeOption $option): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('attribute/' . $attributeKey . '/option/' . $option->getKey())
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }

    /**
     * @throws ApiException
     */
    public function getCustomerAttribute(): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('attribute/customer')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Add new customer attribute
     * @throws ApiException|ExceptionInterface
     */
    public function addCustomerAttribute(CustomerAttribute $customerAttribute): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('attribute/customer')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($this->serializer->normalize($customerAttribute))
            ->request();
    }

    /**
     * @throws ExceptionInterface
     * @throws ApiException
     */
    public function editCustomerAttribute(string $attributeKey, CustomerAttribute $customerAttribute): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('attribute/customer/' . $attributeKey)
            ->setRequestType(Service::REQUEST_METHOD_PUT)
            ->setPostData(['attribute' => $this->serializer->normalize($customerAttribute)])
            ->request();
    }

    /**
     * @throws ApiException
     */
    public function deleteCustomerAttribute(string $attributeKey): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('attribute/customer/' . $attributeKey)
            ->setRequestType(Service::REQUEST_METHOD_DELETE)
            ->request();
    }
}
