<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Campaign;

/**
 * Class CampaignService
 * @package iPresso\Service
 */
class CampaignService
{

    private Service $service;

    /**
     * CampaignService constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Send intentable direct marketing campaign
     * @throws ApiException
     */
    public function send(
        int|string $idCampaign,
        Campaign $campaign,
        bool $noContact = false,
        bool $returnId = false
    ): Response|bool
    {
        /** Przygotowanie parametrów GET */
        $getParam = [];
        if (is_string($idCampaign)) {
            $getParam['key'] = 1;
        }

        if ($noContact) {
            $getParam['nocontact'] = 1;
        }

        if ($returnId) {
            $getParam['returnId'] = 1;
        }

        $queryStr = http_build_query($getParam);

        return $this
            ->service
            ->setRequestPath('campaign/' . $idCampaign . '/send?' . $queryStr)
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($campaign->getCampaign())
            ->request();
    }

    /**
     * @throws ApiException
     */
    public function sendWhatsApp(string $campaignKey, Campaign $campaign, bool $returnId = false): Response|bool
    {
        $getParam = [];

        if ($returnId) {
            $getParam['returnId'] = 1;
        }

        $queryStr = http_build_query($getParam);

        return $this
            ->service
            ->setRequestPath('campaign/whatsapp?' . $queryStr)
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData(array_merge(['campaignKey' => $campaignKey], $campaign->getCampaign()))
            ->request();
    }
}
