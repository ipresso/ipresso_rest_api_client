<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Search;

/**
 * Class SearchService
 * @package iPresso\Service
 */
class SearchService
{

    private Service $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Searching contacts by criteria
     * @throws ApiException
     */
    public function search(Search $search, bool $extended = false): Response|bool
    {
        $path = '';

        if ($extended) {
            $path = '/extended';
        }

        return $this
            ->service
            ->setRequestPath('contact/search' . $path)
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData(['contact' => $search->getCriteria()])
            ->request();
    }

}
