<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\NpsAnswer;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * @package iPresso\Service
 */
class NpsService
{

    private Service $service;

    private Serializer $serializer;

    public function __construct(Service $service, Serializer $serializer)
    {
        $this->service = $service;
        $this->serializer = $serializer;
    }

    /**
     * Get nps data
     * @throws ApiException
     */
    public function get(string $npsApiKey): Response|bool
    {
        return $this
            ->service
            ->setRequestPath("nps/$npsApiKey")
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * @throws ExceptionInterface
     * @throws ApiException
     */
    public function saveAnswer(string $npsApiKey, NpsAnswer $npsAnswer): Response|bool
    {
        $answer = $this->serializer->normalize($npsAnswer);
        return $this
            ->service
            ->setRequestPath("nps/$npsApiKey/answer")
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($answer)
            ->request();
    }
}
