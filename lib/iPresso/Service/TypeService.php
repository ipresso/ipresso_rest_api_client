<?php
declare(strict_types=1);

namespace iPresso\Service;

use iPresso\Exception\ApiException;
use iPresso\Model\Type;

/**
 * Class TypeService
 * @package iPresso\Service
 */
class TypeService
{

    private Service $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * Get all contact types with their dynamic and static attributes
     * @throws ApiException
     */
    public function get(): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('type')
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Add new type of contact
     * @throws ApiException
     */
    public function add(Type $type): Response|bool
    {
        return $this
            ->service
            ->setRequestPath('type')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($type->getType())
            ->request();
    }

    /**
     * Get contacts of a given type
     * @throws ApiException
     */
    public function getContact(int $idType, ?int $page = null): Response|bool
    {
        if ($page) {
            $page = '?page=' . $page;
        }

        return $this
            ->service
            ->setRequestPath('type/' . $idType . '/contact' . $page)
            ->setRequestType(Service::REQUEST_METHOD_GET)
            ->request();
    }

    /**
     * Assignee contact type to a contact
     * @throws ApiException
     */
    public function addContact(string $typeKey, array $contactIds): Response|bool
    {
        if (empty($contactIds)) {
            throw new ApiException('Set idContacts array first.');
        }
        $data = [];
        $data['contact'] = $contactIds;
        return $this
            ->service
            ->setRequestPath('type/' . $typeKey . '/contact')
            ->setRequestType(Service::REQUEST_METHOD_POST)
            ->setPostData($data)
            ->request();
    }
}
