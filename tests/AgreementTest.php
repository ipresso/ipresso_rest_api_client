<?php
declare(strict_types=1);

namespace iPresso\Tests;

use Generator;
use iPresso\Exception\ApiException;
use iPresso\Model\Agreement;

/**
 * Class AgreementTest
 */
class AgreementTest extends ApiTest
{

    /**
     * @see https://apidoc.ipresso.pl/v2/en/#get-all-available-agreements
     * @throws ApiException
     */
    public function testAgreementGetAll(): void
    {
        $response = $this->agreementService->get();

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertEquals(\iPresso\Service\Response::STATUS_OK, $response->getCode());

        $this->assertArrayHasKey('agreement', $response->getData());
    }

    /**
     * @dataProvider invalidAgreementName
     */
    public function testInvalidAgreementAdd(string $name): void
    {
        $category = new Agreement($name);

        //assert
        $this->expectException(ApiException::class);
        $category->getAgreement();

    }

    public function invalidAgreementName(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validAgreementName
     */
    public function testValidAgreementAdd(string $name): void
    {
        $agreement = new \iPresso\Model\Agreement($name);
        $response = $this->agreementService->add($agreement);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);
        $data = $response->getData();

        $this->agreementService->delete($data['agreement']['id']);
    }

    public function validAgreementName(): Generator{
        yield ['abc1234ąęśćżź'];
        yield ['a'];
        yield ['tetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdg'];
    }

    /**
     * @throws ApiException
     * @dataProvider validAgreementName
     */
    public function testValidAgreementAddAll(string $name): void
    {
        $agreement = new \iPresso\Model\Agreement($name);
        $agreement->setDescription('description');
        $agreement->setDmStatus(1);
        $response = $this->agreementService->add($agreement);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);
        $data = $response->getData();

        $this->agreementService->delete($data['agreement']['id']);
    }

    /**
     * @throws ApiException
     */
    public function testCategoryEdit(): void
    {
        $agreement = $this->createAgreementForTest();

        $agreementNew = new Agreement('newName');

        $response = $this->agreementService->edit($agreement, $agreementNew);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);
        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);
        $this->assertTrue($response->getData());

        $this->agreementService->delete($agreement);
    }


    /**
     * @throws ApiException
     */
    public function testAddContactToAgreement(): void
    {
        $contact = $this->createContactForTest();
        $agreement = $this->createAgreementForTest();

        $connect = $this->agreementService->addContact($agreement, [$contact]);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $connect);
        $this->assertTrue($connect->getData());

        $this->assertContains($connect->code, [\iPresso\Service\Response::STATUS_CREATED]);

        $this->agreementService->delete($agreement);
        $this->contactService->delete($contact);
    }

    /**
     * @throws ApiException
     */
    public function testGetContactAgreement(): void
    {
        $contact = $this->createContactForTest();
        $agreement = $this->createAgreementForTest();

        $response = $this->agreementService->getContact($agreement);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);

        $this->assertArrayHasKey('count', $response->getData());

        $this->agreementService->delete($agreement);
        $this->contactService->delete($contact);
    }

    /**
     * @throws ApiException
     */
    public function testDeleteContactAgreement(): void
    {
        $contact = $this->createContactForTest();
        $agreement = $this->createAgreementForTest();

        $connect = $this->agreementService->addContact($agreement, [$contact]);

        $response = $this->agreementService->deleteContact($agreement, (string)$contact);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);

        $this->agreementService->delete($agreement);
        $this->contactService->delete($contact);
    }

    /**
     * @throws ApiException
     */
    public function testCheckContactHasAgreementAfterDelete(): void
    {
        $agreement = $this->createAgreementForTest();
        $contact = $this->createContactForTest();

        $response = $this->agreementService->getContact($agreement);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);

        $data = $response->getData();

        $this->assertArrayHasKey('count', $response->getData());

        if ($data['count'] > 0) {
            $this->assertObjectHasAttribute('id', $data['id']);

            $this->assertNotContains($contact, $data['id']);
        }
    }


}