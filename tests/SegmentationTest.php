<?php
declare(strict_types=1);

namespace iPresso\Tests;

use Generator;
use iPresso\Exception\ApiException;
use iPresso\Model\Segmentation;
use iPresso\Service\Response;

/**
 * Class SegmentationTest
 */
class SegmentationTest extends ApiTest
{

    /**
     * @throws ApiException
     * @dataProvider invalidSegmentationName
     */
    public function testInvalidSegmentationAddName(string $name): void
    {
        $segmentation = new Segmentation(1, $name, 1);

        $this->expectException(ApiException::class);
        $segmentation->getSegmentation();
    }

    private function invalidSegmentationName(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validSegmentationName
     */
    public function testValidSegmentationAddName(string $name): void
    {
        $segmentation = new Segmentation(1, $name, 1);
        $response = $this->segmentationService->add($segmentation);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED, \iPresso\Service\Response::STATUS_FOUND]);
        $data = $response->getData();

        $this->segmentationService->delete($data['id']);
    }

    private function validSegmentationName(): Generator {
        yield ['a'];
        yield ['12345ąęźżćśłó'];
    }


    /**
     * @throws ApiException
     */
    public function testSegmentationGetAll(): void
    {
        $response = $this->segmentationService->get();

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertEquals(\iPresso\Service\Response::STATUS_OK, $response->getCode());

        $this->assertArrayHasKey('segmentation', $response->getData());
    }

    /**
     * @throws ApiException
     */
    public function testAddContactToSegmentation(): void
    {
        $idContact = $this->createContactForTest();
        $idSegmentation = $this->createSegmentationForTest();

        $segmentation = new Segmentation(1, 'testName', 1);
        $segmentation->addContact($idContact);
        $segmentation->setContactOrigin(Segmentation::CONTACT_ORIGIN_ID);

        $response = $this->segmentationService->addContact($idSegmentation, $segmentation);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);
    }

    /**
     * @throws ApiException
     */
    public function testGetContactInSegmentation(): void
    {
        $idContact = $this->createContactForTest();
        $idSegmentation = $this->createSegmentationForTest();

        $segmentation = new Segmentation(1, 'segm1', 1);
        $segmentation->addContact($idContact);
        $this->segmentationService->add($segmentation);
        $this->segmentationService->addContact($idSegmentation, $segmentation);

        $response = $this->segmentationService->getContact($idSegmentation);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);
        $data = $response->getData();
        $this->assertArrayHasKey('contact', $data);

        $this->segmentationService->delete($idSegmentation);
        $this->contactService->delete($idContact);
    }

    /**
     * @depends testAddContactToSegmentation
     * @depends testGetContactInSegmentation
     * @throws ApiException
     */
    public function testRemoveContactFromSegmentation(): void
    {
        $iContactsToRemove = [0, 2];
        $iContactsToRetain = [1];

        $idContacts = [];

        for ($i = 0; $i< count($iContactsToRemove) + count($iContactsToRetain); $i++) {
            $idContacts[] = $this->createContactForTest();
        }

        $idSegmentation = $this->createSegmentationForTest();

        $init_segmentation = new Segmentation(1, 'testInit', 1);
        $init_segmentation->setContact($idContacts);

        $this->segmentationService->addContact($idSegmentation, $init_segmentation);

        $del_segmentation = new Segmentation(1, 'testToRemove', 1);

        foreach ($iContactsToRemove as $i) {
            $del_segmentation->addContact($idContacts[$i]);
        }

        $response = $this->segmentationService->removeContact($idSegmentation, $del_segmentation);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(Response::STATUS_CREATED, $response->getCode());

        $data = $response->getData();
        $this->assertEquals($data['contacts_in_segment'], count($iContactsToRetain));

        $contactsAfter = ($this->segmentationService->getContact($idSegmentation))->getData();

        foreach ($iContactsToRetain as $i) {
            $this->assertArrayHasKey($idContacts[$i], $contactsAfter['contact']);
        }
        foreach ($iContactsToRemove as $i) {
            $this->assertArrayNotHasKey($idContacts[$i], $contactsAfter['contact']);
        }

        foreach ($idContacts as $idContact) {
            $this->contactService->delete($idContact);
        }

        $this->segmentationService->delete($idSegmentation);

    }

    /**
     * @throws ApiException
     */
    public function testDeleteSegmentation(): void
    {
        $idSegmentation = $this->createSegmentationForTest();

        $response = $this->segmentationService->delete($idSegmentation);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);
    }
}