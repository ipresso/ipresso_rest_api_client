<?php
declare(strict_types=1);

namespace iPresso\Tests;

use Generator;
use iPresso\Exception\ApiException;
use iPresso\Model\Form;

class FormTest extends ApiTest
{
    private const TEST_FORM_KEY = '';
    private const FORM_TEST_MODE = false;
    private const TEST_FORM_FIELDS = [];

    /** @dataProvider validFieldNameProvider */
    public function testFormValidationPassed(string $fieldName): void
    {
        $form = new Form([$fieldName => 'value']);

        $form->validate();

        $this->expectNotToPerformAssertions();
    }

    public function validFieldNameProvider(): Generator
    {
        yield ['a_name'];
        yield ['c_name'];
        yield ['v_name'];
        yield ['x_name'];
        yield ['f_name'];
    }

    /** @dataProvider invalidFieldNameProvider */
    public function testFormValidationFail(string|int $fieldName): void
    {
        $form = new Form([$fieldName => 'value']);

        $this->expectException(ApiException::class);

        $form->validate();

        $this->expectNotToPerformAssertions();
    }


    public function invalidFieldNameProvider(): Generator
    {
        yield ['aname'];
        yield ['cname'];
        yield ['vname'];
        yield ['xname'];
        yield ['fname'];
        yield [' '];
        yield [''];
        yield [1];
    }

    public function testFillRequest(): void
    {
        if (empty(self::TEST_FORM_KEY)) {
            $this->expectNotToPerformAssertions();
            return;
        }

        $response = $this->formService->fillForm(
            self::TEST_FORM_KEY,
            new Form(self::TEST_FORM_FIELDS),
            self::FORM_TEST_MODE
        );

        $this->assertEquals(200, $response->code);
    }
}
