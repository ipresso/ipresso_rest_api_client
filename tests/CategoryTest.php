<?php
declare(strict_types=1);

namespace iPresso\Tests;

use Generator;
use iPresso\Exception\ApiException;
use iPresso\Model\Category;

/**
 * Class CategoryTest
 */
class CategoryTest extends ApiTest
{

    /**
     * @dataProvider invalidCategoryName
     */
    public function testInvalidCategoryAdd(string $name): void
    {
        $category = new Category($name);

        //assert
        $this->expectException(ApiException::class);
        $category->getCategory();

    }

    public function invalidCategoryName(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validCategoryName
     */
    public function testValidCategoryAdd(string $name): void
    {
        $category = new \iPresso\Model\Category($name);
        $response = $this->categoryService->add($category);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);
        $data = $response->getData();
        $id = $data['category']['id'];

        $this->categoryService->delete($id);
    }

    public function validCategoryName(): Generator{
        yield['abc1234ąęśćżź'];
        yield['a'];
        yield['tetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdgtetdgeyrgedgrytufjdg'];
    }


    /**
     * @throws ApiException
     */
    public function testCategoryGetAll(): void
    {
        $response = $this->categoryService->get();

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertEquals(\iPresso\Service\Response::STATUS_OK, $response->getCode());

        $this->assertArrayHasKey('category', $response->getData());
    }

    /**
     * @throws ApiException
     */
    public function testValidAddContactToCategory(): void
    {
        $idContact = $this->createContactForTest();
        $idCategory = $this->createCategoryForTest();

        $responseAddContact = $this->categoryService->addContact(
            $idCategory,
            [$idContact]
        );

        $this->assertInstanceOf(\iPresso\Service\Response::class, $responseAddContact);
        $this->assertContains($responseAddContact->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $this->categoryService->deleteContact($idCategory, (string)$idContact);
        $this->categoryService->delete($idCategory);
        $this->contactService->delete($idContact);

    }

    /**
     * @depends testValidAddContactToCategory
     * @throws ApiException
     */
    public function testValidRemoveContactsFromCategory(): void
    {
        $idContact = $this->createContactForTest();
        $idCategory = $this->createCategoryForTest();

        $this->categoryService->addContact(
            $idCategory,
            [$idContact]
        );

        $responseRemoveContact = $this->categoryService->removeContacts(
            $idCategory,
            [$idContact]
        );

        $this->assertInstanceOf(\iPresso\Service\Response::class, $responseRemoveContact);
        $this->assertContains($responseRemoveContact->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $catOnContact = ($this->contactService->getCategory($idContact))->getData();
        $this->assertArrayHasKey('category', $catOnContact);
        $this->assertArrayNotHasKey($idCategory, $catOnContact['category']);

        $this->categoryService->deleteContact($idCategory, (string)$idContact);
        $this->categoryService->delete($idCategory);
        $this->contactService->delete($idContact);

    }

    /**
     * @depends testValidCategoryAdd
     * @throws ApiException
     */
    public function testValidCategoryGet(): void
    {
        $idCategory = $this->createCategoryForTest();

        $response = $this->categoryService->get($idCategory);
        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);
        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);

        $this->assertArrayHasKey('category', $response->getData());

        $this->categoryService->delete($idCategory);
    }


    /**
     * @depends testValidCategoryGet
     * @throws ApiException
     */
    public function testCategoryEdit(): void
    {
        $idCategory = $this->createCategoryForTest();

        $category = new Category('newName');

        $response = $this->categoryService->edit($idCategory, $category);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);
        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);
        $this->assertTrue($response->getData());

        $this->categoryService->delete($idCategory);
    }


    /**
     * @throws ApiException
     */
    public function testGetContactCategory(): void
    {
        $idCategory = $this->createCategoryForTest();

        $response = $this->categoryService->getContact($idCategory);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);
        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);
        $this->assertArrayHasKey('count', $response->getData());

        $this->categoryService->delete($idCategory);
    }

    /**
     * @throws ApiException
     */
    public function testCheckContactHasCategoryAfterDelete(): void
    {
        $idCategory = $this->createCategoryForTest();
        $idContact = $this->createContactForTest();

        $response = $this->categoryService->getContact($idCategory);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);
        $data = $response->getData();

        $this->assertArrayHasKey('count', $data);

        if ($data['count'] > 0) {
            $this->assertArrayHasKey('id', $data);

            $this->assertNotContains($idContact, $data['id']);
        }
        $this->categoryService->delete($idCategory);
        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testCategoryDelete(): void
    {
        $idCategory = $this->createCategoryForTest();

        $response = $this->categoryService->delete($idCategory);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);

        $this->categoryService->delete($idCategory);
    }
}