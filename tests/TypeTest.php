<?php
declare(strict_types=1);

namespace iPresso\Tests;

use Generator;
use iPresso\Exception\ApiException;

/**
 * Class TypeTest
 */
class TypeTest extends ApiTest
{

    /**
     * @throws ApiException
     */
    public function testTypeGetAll(): void
    {
        $response = $this->typeService->get();

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertEquals(\iPresso\Service\Response::STATUS_OK, $response->getCode());

        $this->assertArrayHasKey('type', $response->getData());
    }


    /**
     * @throws ApiException
     * @dataProvider invalidTypeName
     */
    public function testInvalidTypeAddName(string $name): void
    {
        $key = 'key12345';
        $type = new \iPresso\Model\Type($key, $name);

        $this->expectException(ApiException::class);
        $type->getType();
    }

    private function invalidTypeName(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validTypeName
     */
    public function testValidTypeAddName(string $name): void
    {
        $key = 'key1234';
        $type = new \iPresso\Model\Type($key, $name);
        $response = $this->typeService->add($type);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED, \iPresso\Service\Response::STATUS_FOUND]);
    }

    private function validTypeName(): Generator {
        yield ['a'];
        yield ['12345ąęźżćśłó'];
    }

    /**
     * @throws ApiException
     * @dataProvider invalidTypeKey
     */
    public function testInvalidTypeAddKey(string $key): void
    {
        $name = 'name';
        $type = new \iPresso\Model\Type($key, $name);

        $this->expectException(ApiException::class);
        $type->getType();
    }

    private function invalidTypeKey(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validTypeKey
     */
    public function testValidTypeAddKey(string $key): void
    {
        $name = 'name';
        $type = new \iPresso\Model\Type($key, $name);
        $response = $this->typeService->add($type);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED, \iPresso\Service\Response::STATUS_FOUND]);
    }

    private function validTypeKey(): Generator {
        yield ['a'];
    }

    /**
     * @throws ApiException
     */
    public function testAddContactToType(): void
    {
        $typeKey = $this->createTypeForTest();
        $idContact = $this->createContactForTest();

        $this->assertNotEmpty($typeKey);

        $response = $this->typeService->addContact($typeKey, [$idContact]);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED, \iPresso\Service\Response::STATUS_FOUND, \iPresso\Service\Response::STATUS_OK]);

        $this->contactService->delete($idContact);
    }
}
