<?php
declare(strict_types=1);

namespace iPresso\Tests;

use Generator;
use iPresso\Exception\ApiException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class ActivityTest
 */
class ActivityTest extends ApiTest
{

    /**
     * @throws ApiException
     * @dataProvider invalidActivityName
     */
    public function testInvalidActivityAddName(string $name): void
    {
        $key = 'key12345';
        $activity = new \iPresso\Model\Activity($key, $name);

        $this->expectException(ApiException::class);
        $activity->getActivity();
    }

    private function invalidActivityName(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validActivityName
     */
    public function testValidActivityAddName(string $name): void
    {
        $key = 'key1234';
        $activity = new \iPresso\Model\Activity($key, $name);
        $response = $this->activityService->add($activity);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $this->activityService->delete($activity->getKey());
    }

    private function validActivityName(): Generator {
        yield ['a'];
        yield ['12345ąęźżćśłó'];
    }

    /**
     * @throws ApiException
     * @dataProvider invalidActivityKey
     */
    public function testInvalidActivityAddKey(string $key): void
    {
        $name = 'name';
        $activity = new \iPresso\Model\Activity($key, $name);

        $this->expectException(ApiException::class);
        $activity->getActivity();
    }

    private function invalidActivityKey(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validActivityKey
     */
    public function testValidActivityAddKey(string $key): void
    {
        $name = 'name';
        $activity = new \iPresso\Model\Activity($key, $name);
        $response = $this->activityService->add($activity);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $this->activityService->delete($activity->getKey());
    }

    private function validActivityKey(): Generator {
        yield ['a'];
    }

    /**
     * @throws ApiException
     */
    public function testActivityGetAll(): void
    {
        $response = $this->activityService->get();

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertEquals(\iPresso\Service\Response::STATUS_OK, $response->getCode());
        $this->assertArrayHasKey('activity', $response->getData());
    }

    /**
     * @throws ExceptionInterface
     * @throws ApiException
     */
    public function testActivityEdit(): void
    {
        $activity = $this->createActivityForTest();
        $activityNew = new \iPresso\Model\Activity('newKey', 'newName');

        $response = $this->activityService->edit($activity, $activityNew);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $this->assertTrue($response->getData());

        $this->activityService->delete($activity);
    }

    /**
     * @throws ApiException
     */
    public function testAddContactToActivity(): void
    {
        $contact = $this->createContactForTest();
        $activity = $this->createActivityForTest();

        $connect = $this->activityService->addContact($activity, [$contact]);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $connect);
        $this->assertTrue($connect->getData());

        $this->assertContains($connect->code, [\iPresso\Service\Response::STATUS_CREATED]);

        $this->activityService->delete($activity);
        $this->contactService->delete($contact);
    }
}