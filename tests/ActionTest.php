<?php
declare(strict_types=1);

namespace iPresso\Tests;

use Generator;
use iPresso\Exception\ApiException;

/**
 * Class ActionTest
 */
class ActionTest extends ApiTest
{
    /**
     * @throws ApiException
     * @dataProvider invalidActionName
     */
    public function testInvalidActionAddName(string $name): void
    {
        $key = '1234key12345';
        $action = new \iPresso\Model\Action($key, $name);

        $this->expectException(ApiException::class);
        $action->getAction();
    }

    private function invalidActionName(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validActionName
     */
    public function testValidActionAddName(string $name): void
    {
        $key = '1234key1234';
        $action = new \iPresso\Model\Action($key, $name);
        $response = $this->actionService->add($action);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $this->actionService->delete($action->getKey());
    }

    private function validActionName(): Generator {
        yield ['a'];
        yield ['12345ąęźżćśłó'];
    }

    /**
     * @throws ApiException
     * @dataProvider invalidActionKey
     */
    public function testInvalidActionAddKey(string $key): void
    {
        $name = 'name';
        $action = new \iPresso\Model\Action($key, $name);

        $this->expectException(ApiException::class);
        $action->getAction();
    }

    private function invalidActionKey(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validActionKey
     */
    public function testValidActionAddKey(string $key): void
    {
        $name = '1234name';
        $action = new \iPresso\Model\Action($key, $name);
        $response = $this->actionService->add($action);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $this->actionService->delete($action->getKey());
    }

    private function validActionKey(): Generator {
        yield ['a'];
        yield ['12345ae'];
    }

    /**
     * @throws ApiException
     */
    public function testActionGetAll(): void
    {
        $response = $this->actionService->get();

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertEquals(\iPresso\Service\Response::STATUS_OK, $response->getCode());

        $this->assertArrayHasKey('action', $response->getData());
    }

    /**
     * @throws ApiException
     */
    public function testActionEdit(): void
    {
        $action = $this->createActionForTest();
        $actionNew = new \iPresso\Model\Action('newKey', 'newName');

        $response = $this->actionService->edit($action, $actionNew);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $this->actionService->delete($action);
    }

    /**
     * @throws ApiException
     */
    public function testAddContactToAction(): void
    {
        $contact = $this->createContactForTest();
        $action = $this->createActionForTest();

        $connect = $this->actionService->addContact($action, [$contact]);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $connect);

        $this->assertContains($connect->code, [\iPresso\Service\Response::STATUS_CREATED]);

        $this->actionService->delete($action);
        $this->contactService->delete($contact);
    }

}