<?php
declare(strict_types=1);

namespace iPresso\Tests;

use Generator;
use iPresso\Exception\ApiException;

/**
 * Class TagTest
 */
class TagTest extends ApiTest
{
    /**
     * @throws ApiException
     * @dataProvider invalidTagName
     */
    public function testInvalidTagAddName(string $name): void
    {
        $tag = new \iPresso\Model\Tag($name);

        $this->expectException(ApiException::class);
        $tag->getTag();
    }

    private function invalidTagName(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validTagName
     */
    public function testValidTagAddName(string $name): void
    {
        $tag = new \iPresso\Model\Tag($name);
        $response = $this->tagService->add($tag);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED, \iPresso\Service\Response::STATUS_FOUND]);
        $data = $response->getData();

        $this->tagService->delete($data['tag']['id']);
    }

    private function validTagName(): Generator {
        yield ['a'];
        yield ['12345ąęźżćśłó'];
    }

    /**
     * @throws ApiException
     */
    public function testTagGetAll(): void
    {
        $response = $this->tagService->get();

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertEquals(\iPresso\Service\Response::STATUS_OK, $response->getCode());

        $this->assertArrayHasKey('tag', $response->getData());
    }

    /**
     * @throws ApiException
     */
    public function testTagGet(): void
    {
        $tag = $this->createTagForTest();

        $response = $this->tagService->get($tag);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);

        $this->assertArrayHasKey('tag', $response->getData());

        $this->tagService->delete($tag);
    }

    /**
     * @throws ApiException
     */
    public function testTagEdit(): void
    {
        $idTag = $this->createTagForTest();
        $tag = new \iPresso\Model\Tag('nazwaEdytowana');

        $response = $this->tagService->edit($idTag, $tag);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);

        $this->assertTrue($response->getData());

        $this->tagService->delete($idTag);
    }


    /**
     * @throws Exception
     */
    public function testContactAdd(): void
    {
        $contact = new \iPresso\Model\Contact();
        $contact->setEmail('dominika.dubaniowska.test@encja.com');

        $response = $this->contactService->add($contact);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK, \iPresso\Service\Response::STATUS_CREATED, \iPresso\Service\Response::STATUS_FOUND,]);

        $data = $response->getData();

        $this->assertArrayHasKey('contact', $data);

        $this->contactService->delete((int)$data['contact']['1']['id']);
    }

    /**
     * @throws ApiException
     */
    public function testAddContactToTag(): void
    {
        $idTag = $this->createTagForTest();
        $idContact = $this->createContactForTest();

        $response = $this->tagService->addContact($idTag, [$idContact]);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $this->tagService->delete($idTag);
        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testGetContactTag(): void
    {
        $idTag = $this->createTagForTest();
        $idContact = $this->createContactForTest();

        $response = $this->tagService->getContact($idTag);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);
        $data = $response->getData();

        $this->assertArrayHasKey('count', $data);

        $this->tagService->delete($idTag);
        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testDeleteContactTag(): void
    {
        $idTag = $this->createTagForTest();
        $idContact = $this->createContactForTest();

        $responseAdd = $this->tagService->addContact($idTag, [$idContact]);
        $response = $this->tagService->deleteContact($idTag, (string)$idContact);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);

        $this->tagService->delete($idTag);
        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testCheckContactHasTagAfterDelete(): void
    {
        $idTag = $this->createTagForTest();
        $idContact = $this->createContactForTest();

        $response = $this->tagService->getContact($idTag);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);

        $data = $response->getData();

        $this->assertArrayHasKey('count', $response->getData());

        if ($data['count'] > 0) {
            $this->assertObjectHasAttribute('id', $data['id']);

            $this->assertNotContains($idContact, $data['id']);
        }

        $this->tagService->delete($idTag);
        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testTagDelete(): void
    {
        $idTag = $this->createTagForTest();

        $response = $this->tagService->delete($idTag);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);
    }
}