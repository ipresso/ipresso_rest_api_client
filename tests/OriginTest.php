<?php
declare(strict_types=1);

namespace iPresso\Tests;

use iPresso\Exception\ApiException;

/**
 * Class OriginTest
 */
class OriginTest extends ApiTest
{

    /**
     * @throws ApiException
     */
    public function testOriginGetAll()
    {
        $response = $this->originService->get();

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertEquals(\iPresso\Service\Response::STATUS_OK, $response->getCode());

        $this->assertArrayHasKey('origin', $response->getData());
    }

    /**
     * @throws ApiException
     */
    public function testGetContactOrigin()
    {
        $idOrigin = 1;
        $this->assertGreaterThan(0, $idOrigin);

        $response = $this->originService->getContact($idOrigin);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);
    }
}