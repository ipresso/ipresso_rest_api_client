<?php
declare(strict_types=1);

namespace iPresso\Tests;

use iPresso\Exception\ApiException;
use iPresso\Model\Action;
use iPresso\Model\Activity;
use iPresso\Model\Agreement;
use iPresso\Model\Contact;
use iPresso\Model\ContactAction;
use iPresso\Model\ContactActivity;
use iPresso\Model\MassContactActivity;
use iPresso\Service\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class OriginTest
 */
class ContactTest extends ApiTest
{
    /**
     * @throws ApiException
     */
    public function testContactAdd(): void
    {
        $contact = new Contact();
        $contact->setEmail('dominika.dubaniowska.test@encja.com');

        $response = $this->contactService->add($contact);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_OK]);

        $data = $response->getData();

        $this->assertArrayHasKey('contact', $data);

        $this->contactService->delete((int)$data['contact']['1']['id']);
    }

    /**
     * @throws ApiException
     */
    public function testContactAddAll(): void
    {
        $contact = new Contact();
        $contact
            ->setEmail('dominika.dubaniowska.test@encja.com')
            ->setFirstName('Marek')
            ->setLastName('Automatek')
            ->setBuildingNumber('4')
            ->setCity('Katowice')
            ->setCountry('Polska')
            ->setFlatNumber('12')
            ->setCompany('Ipresso')
            ->setWorkPosition('Developer')
            ->setMobile('123')
            ->setPostCode('43-500')
            ->setStreet('Ceglana')
            ->setRegion('Slask');

        $response = $this->contactService->add($contact);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_OK]);

        $data = $response->getData();

        $this->assertArrayHasKey('contact', $data);

        $this->contactService->delete((int)$data['contact']['1']['id']);
    }

    /**
     * @throws ApiException
     */
    public function testContactGet(): void
    {
        $idContact = $this->createContactForTest();

        $response = $this->contactService->get($idContact);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_OK]);

        $data = $response->getData();

        $this->assertArrayHasKey('contact', $data);

        $this->assertEquals($idContact, $data['contact']['idContact']);

        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testContactEdit(): void
    {
        $idContact = $this->createContactForTest();

        $contact = new Contact();
        $contact->setFirstName('Dominika');
        $contact->setLastName('Dubaniowska');

        $response = $this->contactService->edit($idContact, $contact);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_CREATED]);

        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testContactAddAction(): void
    {
        $contact = $this->createContactForTest();
        $action = new Action('1234key', 'name');

        $contactAction = new ContactAction('1234key');
        $this->actionService->add($action);

        $response = $this->contactService->addAction($contact, $contactAction);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_CREATED]);

        $this->contactService->delete($contact);
    }

    /**
     * @throws ApiException
     */
    public function testContactAddActivity(): void
    {
        $contact = $this->createContactForTest();
        $activityKey = 'key123456';
        $activity = new Activity($activityKey, 'test');
        $contactActivity = new ContactActivity($activityKey);

        $this->activityService->add($activity);
        $response = $this->contactService->addActivity($contact, $contactActivity);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_CREATED]);

        $this->activityService->delete($activityKey);
        $this->contactService->delete($contact);
    }

    /**
     * @throws ApiException
     * @throws ExceptionInterface
     */
    public function testContactAddActivities(): void
    {
        $contact = $this->createContactForTest();
        $activity = $this->createActivityForTest();

        $activities = new MassContactActivity([$activity]);

        $response = $this->contactService->addMassActivity($activities);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_CREATED]);

        $this->activityService->delete($activity);
        $this->contactService->delete($contact);
    }


    /**
     * @throws ApiException
     */
    public function testContactAddAgreement(): void
    {
        $agreement = $this->createAgreementForTest();
        $contact = $this->createContactForTest();

        $response = $this->contactService->addAgreement($contact, [$agreement => Agreement::DIRECT_MARKETING_VISIBLE]);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_CREATED]);

        $this->agreementService->delete($agreement);
        $this->contactService->delete($contact);
    }

    /**
     * @throws ApiException
     */
    public function testContactGetAgreement(): void
    {
        $contact = $this->createContactForTest();
        $agreement = $this->createAgreementForTest();

        $responseAdd = $this->contactService->addAgreement($contact, [$agreement => Agreement::DIRECT_MARKETING_VISIBLE]);
        $responseGet = $this->contactService->getAgreement($contact);

        $this->assertInstanceOf(Response::class, $responseGet);

        $this->assertContains($responseGet->getCode(), [Response::STATUS_OK]);

        $this->assertArrayHasKey('agreement', $responseGet->getData());

        $this->assertNotEmpty($agreement);

        $this->agreementService->delete($agreement);
        $this->contactService->delete($contact);
    }

    /**
     * @throws ApiException
     */
    public function testContactGetCategory(): void
    {
        $idCategory = $this->createCategoryForTest();
        $idContact = $this->createContactForTest();
        $this->contactService->addCategory($idContact, [$idCategory]);

        $response = $this->contactService->getCategory($idContact);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_OK]);

        $this->assertArrayHasKey('category', $response->getData());

        $this->assertNotEmpty($idCategory);

        $this->categoryService->delete($idCategory);
        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testContactAddCategory(): void
    {
        $idCategory = $this->createCategoryForTest();
        $idContact = $this->createContactForTest();

        $response = $this->contactService->addCategory($idContact, [$idCategory]);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_CREATED]);

        $this->categoryService->delete($idCategory);
        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testContactDeleteCategory(): void
    {
        $idCategory = $this->createCategoryForTest();
        $idContact = $this->createContactForTest();

        $response = $this->contactService->deleteCategory($idContact, $idCategory);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_OK]);

        $this->categoryService->delete($idCategory);
        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testContactAddTag(): void
    {
        $idContact = $this->createContactForTest();
        $tag = 'tagString';

        $response = $this->contactService->addTag($idContact, $tag);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_CREATED]);

        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testContactGetTag(): void
    {
        $idContact = $this->createContactForTest();
        $tag = 'tagString';

        $responseAdd = $this->contactService->addTag($idContact, $tag);
        $response = $this->contactService->getTag($idContact);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_OK]);
        $data = $response->getData();

        $this->assertArrayHasKey('tag', $data);

        $idTag = array_search($tag, (array)$data['tag']);

        $this->contactService->delete($idContact);
    }

    /**
     * @throws ApiException
     */
    public function testContactDeleteTag(): void
    {
        $idTag = $this->createTagForTest();
        $idContact = $this->createContactForTest();

        $responseAdd = $this->contactService->addTag($idContact, 'nameTest');
        $response = $this->contactService->deleteTag($idContact, $idTag);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_OK]);

        $response = $this->tagService->delete($idTag);

        $this->assertInstanceOf(Response::class, $response);

        $this->assertContains($response->getCode(), [Response::STATUS_OK]);
    }

}
