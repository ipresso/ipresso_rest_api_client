<?php
declare(strict_types=1);

namespace iPresso\Tests;

use iPresso;
use iPresso\Exception\ApiException;
use iPresso\Service\Service;

class ServiceTest extends ApiTest
{
    private \iPresso\Service\Service $service;
    protected function setUp(): void
    {
        $this->service = new Service();
        $this->service->setLogin(ConnConstrains::LOGIN)
            ->setPassword(ConnConstrains::PASSWORD)
            ->setUrl(ConnConstrains::URL)
            ->setCustomerKey(ConnConstrains::CUSTOMER_KEY)
            ->getToken();
        parent::setUp();
    }

    /**
     * @throws ApiException
     */
    public function testSetWrongUrl(): void
    {
        $this->expectException(ApiException::class);
        $class = new iPresso();
        $class->setUrl('https://panel.ipresso.dev');
    }

    /**
     * @throws ApiException
     */
    public function testGetToken(): void
    {
        $token = $this->service->getToken();

        $this->assertNotEmpty($token);
    }

}