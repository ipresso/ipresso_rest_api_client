<?php
declare(strict_types=1);

namespace iPresso\Tests;

use Generator;
use iPresso\Exception\ApiException;

/**
 * Class WebsiteTest
 */
class WebsiteTest extends ApiTest
{

    /**
     * @dataProvider invalidUrl
     */
    public function testInvalidWebsiteAdd($url)
    {
        $this->expectException(ApiException::class);
        $this->websiteService->add($url);
    }

    public function invalidUrl(): Generator {
        yield ['test'];
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validUrl
     */
    public function testValidTagAddName(string $name): void
    {
        $response = $this->websiteService->add($name);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);
        $data = $response->getData();

        $this->websiteService->delete($data['www']['id']);
    }

    public function validUrl(): Generator {
        yield ['https://test.com'];
    }

    /**
     * @throws ApiException
     */
    public function testWebsiteGetAll(): void
    {
        $idWebsite = $this->createWebsiteForTest();
        $response = $this->websiteService->get();

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);
        $this->assertEquals(\iPresso\Service\Response::STATUS_OK, $response->getCode());

        $data = $response->getData();
        $this->assertArrayHasKey('www', $data);
        $this->assertArrayHasKey($idWebsite, $data['www']);

        $this->websiteService->delete($idWebsite);
    }

    /**
     * @throws ApiException
     */
    public function testWebsiteAdd(): void
    {
        $response = $this->websiteService->add('https://test.pl');

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED, \iPresso\Service\Response::STATUS_FOUND]);
        $data = $response->getData();

        $this->assertArrayHasKey('www', $data);

        $this->websiteService->delete((int)$data['www']['id']);
    }

    /**
     * @throws ApiException
     */
    public function testWebsiteGet(): void
    {
        $idWebsite = $this->createWebsiteForTest();

        $response = $this->websiteService->get($idWebsite);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK, \iPresso\Service\Response::STATUS_FOUND]);

        $data = $response->getData();
        $this->assertArrayHasKey('www', $data);

        $this->assertEquals($idWebsite, $data['www']['id']);

        $this->websiteService->delete($idWebsite);
    }

}