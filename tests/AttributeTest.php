<?php
declare(strict_types=1);

namespace iPresso\Tests;

use Generator;
use iPresso\Exception\ApiException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class AttributeTest
 */
class AttributeTest extends ApiTest
{

    /**
     * @throws ApiException
     * @dataProvider invalidAttributeName
     */
    public function testInvalidAttributeAddName(string $name): void
    {
        $key = 'key12345';
        $type = \iPresso\Model\Attribute::TYPE_MULTI_SELECT;
        $attribute = new \iPresso\Model\Attribute($key, $name, $type);

        $this->expectException(ApiException::class);
        $attribute->getAttribute();
    }

    private function invalidAttributeName(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validAttributeName
     */
    public function testValidAttributeAddName(string $name): void
    {
        $key = 'key1234';
        $type = \iPresso\Model\Attribute::TYPE_MULTI_SELECT;
        $attribute = new \iPresso\Model\Attribute($key, $name, $type);
        $response = $this->attributeService->add($attribute);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED, \iPresso\Service\Response::STATUS_FOUND]);

        $this->attributeService->deleteCustomerAttribute($attribute->getKey());
    }

    private function validAttributeName(): Generator {
        yield ['a'];
        yield ['12345ąęźżćśłó'];
    }

    /**
     * @throws ApiException
     * @dataProvider invalidAttributeKey
     */
    public function testInvalidAttributeAddKey(string $key):void
    {
        $name = 'name';
        $type = \iPresso\Model\Attribute::TYPE_MULTI_SELECT;
        $activity = new \iPresso\Model\Attribute($key, $name, $type);

        $this->expectException(ApiException::class);
        $activity->getAttribute();
    }

    private function invalidAttributeKey(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validAttributeKey
     */
    public function testValidAttributeAddKey(string $key): void
    {
        $name = 'name';
        $type = \iPresso\Model\Attribute::TYPE_MULTI_SELECT;
        $attribute = new \iPresso\Model\Attribute($key, $name, $type);
        $response = $this->attributeService->add($attribute);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED, \iPresso\Service\Response::STATUS_FOUND]);

        $this->attributeService->deleteCustomerAttribute($attribute->getKey());
    }

    private function validAttributeKey(): Generator {
        yield ['a'];
    }

    /**
     * @throws ApiException
     */
    public function testAttributeGetAll(): void
    {
        $response = $this->attributeService->get();

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertEquals(\iPresso\Service\Response::STATUS_OK, $response->getCode());

        $this->assertArrayHasKey('attribute', $response->getData());
    }

    /**
     * @throws ApiException
     * @dataProvider invalidAttributeOptionKey
     */
    public function testInvalidAttributeOptionAddKey(string $key): void
    {
        $name = 'name';
        $type = \iPresso\Model\Attribute::TYPE_MULTI_SELECT;
        $activity = new \iPresso\Model\Attribute($key, $name, $type);

        $this->expectException(ApiException::class);
        $activity->getAttribute();
    }

    private function invalidAttributeOptionKey(): Generator {
        yield [''];
    }

    /**
     * @throws ApiException
     * @dataProvider validAttributeOptionKey
     */
    public function testValidAttributeOptionAddKey(string $key): void
    {
        $attribute = $this->createAttributeForTest();

        $value = 'value';
        $attributeOption = new \iPresso\Model\AttributeOption($key, $value);
        $response = $this->attributeService->addOption($attribute, $attributeOption);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $this->attributeService->deleteCustomerAttribute($attribute);
    }

    private function validAttributeOptionKey(): Generator {
        yield ['a'];
        yield ['12345ąęźżćśłó'];
    }

    /**
     * @throws ExceptionInterface
     * @throws ApiException
     */
    public function testAttributeOptionEdit(): void
    {
        $keys = $this->createAttributeOptionForTest();

        $option = new \iPresso\Model\AttributeOption($keys[0], 'valueTest2');

        $response = $this->attributeService->editOption($keys[1], $option);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_CREATED]);

        $this->attributeService->deleteOption($keys[1], $option);
        $this->attributeService->deleteCustomerAttribute($keys[1]);
    }

    /**
     * @throws ApiException
     */
    public function testAttributeOptionDelete(): void
    {
        $attribute = $this->createAttributeForTest();

        $attributeOption = new \iPresso\Model\AttributeOption('1234keyTest', 'valueTest');
        $response = $this->attributeService->addOption($attribute, $attributeOption);

        $response = $this->attributeService->deleteOption($attribute, $attributeOption);

        $this->assertInstanceOf(\iPresso\Service\Response::class, $response);

        $this->assertContains($response->getCode(), [\iPresso\Service\Response::STATUS_OK]);
        $this->attributeService->deleteCustomerAttribute($attribute);
    }

}