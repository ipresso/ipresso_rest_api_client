<?php
declare(strict_types=1);

namespace iPresso\Tests;

use iPresso\Exception\ApiException;
use iPresso\Model\Action;
use iPresso\Model\Attribute;
use iPresso\Model\Form;
use iPresso\Model\Segmentation;
use iPresso\Service\ActionService;
use iPresso\Service\ActivityService;
use iPresso\Service\AgreementService;
use iPresso\Service\AttributeService;
use iPresso\Service\CategoryService;
use iPresso\Service\ContactService;
use iPresso\Service\FormService;
use iPresso\Service\OriginService;
use iPresso\Service\SegmentationService;
use iPresso\Service\Service;
use iPresso\Service\TagService;
use iPresso\Service\TypeService;
use iPresso\Service\WebsiteService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

abstract class ApiTest extends TestCase
{
    protected ActivityService $activityService;
    protected ContactService $contactService;
    protected ActionService $actionService;
    protected CategoryService $categoryService;
    protected AgreementService $agreementService;
    protected AttributeService $attributeService;
    protected SegmentationService $segmentationService;
    protected TagService $tagService;
    protected TypeService $typeService;
    protected WebsiteService $websiteService;
    protected OriginService $originService;
    protected FormService $formService;

    protected function setUp(): void
    {
        $service = new Service();
        $service->setLogin(ConnConstrains::LOGIN)
            ->setPassword(ConnConstrains::PASSWORD)
            ->setUrl(ConnConstrains::URL)
            ->setCustomerKey(ConnConstrains::CUSTOMER_KEY)
            ->getToken();
        $serializer = \iPresso::buildSerializer();
        $this->activityService = new ActivityService($service, $serializer);
        $this->contactService = new ContactService($service, $serializer);
        $this->agreementService = new AgreementService($service);
        $this->actionService = new ActionService($service, $serializer);
        $this->attributeService = new AttributeService($service, $serializer);
        $this->categoryService = new CategoryService($service);
        $this->segmentationService = new SegmentationService($service);
        $this->tagService = new TagService($service);
        $this->typeService = new TypeService($service);
        $this->originService = new OriginService($service);
        $this->websiteService = new WebsiteService($service);
        $this->formService= new FormService($service, $serializer);
    }

    /**
     * @throws ApiException
     */
    protected function createActionForTest(): string
    {
        $name = '1234nameTest';
        $key = '1234abc';
        $action = new Action($key, $name);
        $this->actionService->add($action);

        return $key;
    }

    /**
     * @throws ApiException
     */
    protected function createCategoryForTest(): int
    {
        $name = 'nameTest';
        $category = new \iPresso\Model\Category($name);
        $responseCategory = $this->categoryService->add($category);
        $data = $responseCategory->getData();

        return (int)$data['category']['id'];
    }

    /**
     * @throws ApiException
     */
    protected function createContactForTest(): int
    {
        $microtime = substr(microtime(),2,6);
        $email = "dominika.d.test.$microtime@encja.com";
        $contact = new \iPresso\Model\Contact($email);
        $responseContact = $this->contactService->add($contact);
        $data = $responseContact->getData();

        return (int)$data['contact']['1']['id'];
    }

    /**
     * @throws ApiException
     */
    protected function createActivityForTest(): string
    {
        $name = '123ameTest';
        $key = '12abc';
        $activity = new \iPresso\Model\Activity($key, $name);
        $this->activityService->add($activity);

        return $key;
    }

    /**
     * @throws ApiException
     */
    protected function createAgreementForTest(): int
    {
        $name = 'nameTest';
        $agreement = new \iPresso\Model\Agreement($name);
        $responseAgreement = $this->agreementService->add($agreement);
        $data = $responseAgreement->getData();

        return (int)$data['agreement']['id'];
    }

    /**
     * @throws ApiException
     */
    protected function createAttributeForTest(): string
    {
        $name = 'nameTest';
        $key = 'abc';
        $type = Attribute::TYPE_MULTI_SELECT;
        $attribute = new Attribute($key, $name, $type);
        $this->attributeService->add($attribute);

        return $key;
    }

    /**
     * @throws ApiException
     */
    protected function createAttributeOptionForTest(): array
    {
        $key = '1234key1234';
        $attrKey = 'key123456';
        $attribute = new Attribute($attrKey, 'name1234', Attribute::TYPE_SELECT);
        $attributeOption = new \iPresso\Model\AttributeOption($key, 'value12345');
        $this->attributeService->add($attribute);
        $this->attributeService->addOption($attribute->getKey(), $attributeOption);

        return [$key, $attrKey];
    }

    /**
     * @throws ApiException
     */
    protected function createSegmentationForTest(): int
    {
        $name = 'nameTest';
        $segmentation = new Segmentation(1, $name,1);
        $responseType = $this->segmentationService->add($segmentation);
        $data = $responseType->getData();

        return $data['id'];
    }

    /**
     * @throws ApiException
     */
    protected function createTagForTest(): int
    {
        $name = 'nameTest';
        $tag = new \iPresso\Model\Tag($name);
        $responseType = $this->tagService->add($tag);
        $data = $responseType->getData();

        return $data['tag']['id'];
    }

    /**
     * @throws ApiException
     */
    protected function createTypeForTest(): string
    {
        $name = 'restApiClientTest';
        $key = 'restApiClientTest';
        $type = new \iPresso\Model\Type($key, $name);
        $this->typeService->add($type);

        return $key;
    }

    /**
     * @throws ApiException
     */
    protected function createWebsiteForTest(): int
    {
        $url = 'https://test.pl';
        $responseWebsite = $this->websiteService->add($url);

        $data = $responseWebsite->getData();

        return (int)$data['www']['id'];
    }

}
