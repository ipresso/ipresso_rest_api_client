# iPresso REST API Client
[![Total Downloads](https://img.shields.io/packagist/dt/encjacom/ipresso_api.svg)](https://packagist.org/packages/encjacom/ipresso_api/)
[![Latest Stable Version](https://img.shields.io/packagist/v/encjacom/ipresso_api.svg)](https://packagist.org/packages/encjacom/ipresso_api/)

## Documentation

- [API Documentation](https://apidoc.ipresso.com/)

## Installation

Install the latest version with

```bash
$ composer require encjacom/ipresso_api
```
## Examples

### Authentication

```php
<?php

$ipresso = new iPresso();
$ipresso->setLogin('login');
$ipresso->setPassword('password');
$ipresso->setCustomerKey('customerKey');
$ipresso->setUrl('https://yourdomain.ipresso.pl');
$token = $ipresso->getToken();
$ipresso->setToken($token->data);
```

## Other examples

- [Contact Actions](doc/action.md)
- [Contact Activities](doc/activity.md)
- [Consents](doc/agreement.md)
- [Contact and Customer Attributes](doc/attribute.md)
- [Campaigns](doc/campaign.md)
- [Categories](doc/category.md)
- [Contacts](doc/contact.md)
- [Anonymous Contacts](doc/contactAnonymous.md)
- [NPS](doc/nps.md)
- [Origins](doc/origin.md)
- [Scenarios](doc/scenario.md)
- [Search](doc/search.md)
- [Segmentations](doc/segmentation.md)
- [Tags](doc/tag.md)
- [Types](doc/type.md)
- [Websites](doc/website.md)

## About

### Requirements

- iPresso REST API Client works with PHP 8.1 or above.

### Submitting bugs and feature requests

Bugs and feature request are tracked on [GitHub](https://github.com/encjacom/ipresso_api/issues)
